<%@ page import="com.mv.utils.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String error_message = (session.getAttribute(Constants.ERROR_MESSAGE) == null) ? "" : (String)session.getAttribute(Constants.ERROR_MESSAGE);
    session.removeAttribute(Constants.ERROR_MESSAGE);
%>

<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>e-voting login</title>

    <link rel="shortcut icon" href="img/icons/favicon.ico"/>
    <!-- Vendor CSS -->
    <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
    <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

    <!-- CSS -->
    <link href="css/app_1.min.css" rel="stylesheet">
    <link href="css/app_2.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="assert/css/color.css" rel="stylesheet">
    <link href="assert/css/fonts.css" rel="stylesheet">
</head>

<body>
<div class="login-content bg-purple-50">
    <!-- Login -->
    <div class="lc-block toggled" id="l-login">
        <form action="log-in" method="post" id="login_form">
            <div class="lcb-form">
                <div class="input-group m-b-30">
                    <span class="input-group-addon"><i class="zmdi f-30 zmdi-account"></i></span>
                    <div class="fg-line">
                        <input type="text" name="username" class="form-control input-lg" placeholder="Username">
                    </div>
                </div>

                <div class="input-group m-b-30">
                    <span class="input-group-addon"><i class="zmdi f-30 zmdi-male"></i></span>
                    <div class="fg-line">
                        <input type="password" name="password" class="form-control input-lg" placeholder="Password">
                    </div>
                </div>
                <div style="text-align: center" class="checkbox">
                    <label style="color: red; padding: 0">
                        <%--<input type="checkbox" value="">
                        <i class="input-helper"></i>--%>
                        <%=error_message%>
                    </label>
                </div>

                <a href="javascript:void(0);" onclick="$('#login_form').submit()" class="btn btn-login  bgm-deeppurple btn-float">
                    <i class="zmdi zmdi-arrow-forward"></i>
                </a>
                <input type="submit" style="visibility: hidden">
            </div>
        </form>

        <%--<div class="lcb-navigation">
            <a href="login.html" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>
            <a href="login.html" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
        </div>--%>
    </div>

    <!-- Register -->
    <%--<div class="lc-block" id="l-register">
        <div class="lcb-form">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" placeholder="Username">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" placeholder="Email Address">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" placeholder="Password">
                </div>
            </div>

            <a href="login.html" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-check"></i></a>
        </div>

        <div class="lcb-navigation">
            <a href="login.html" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
            <a href="login.html" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
        </div>
    </div>--%>

    <!-- Forgot Password -->
    <%--<div class="lc-block" id="l-forget-password">
        <div class="lcb-form">
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu risus. Curabitur commodo lorem fringilla enim feugiat commodo sed ac lacus.</p>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" placeholder="Email Address">
                </div>
            </div>

            <a href="login.html" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-check"></i></a>
        </div>

        <div class="lcb-navigation">
            <a href="login.html" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
            <a href="login.html" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>
        </div>
    </div>--%>
</div>

<footer style="padding-left:0; height: 170px;" id="footer">

    <div class="row">
        <div class="col-md-6">
            <ul class="list-style-none display-block pull-left">
                <li><img style="height: 80px;" src="img/icons/logo1.png"/></li>
                <li><p class="f-16">Azerbaijan 2017 41st World Scout Conference </br>
                    13th World Scout Youth Forum</p></li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="list-style-none display-block pull-right">
                <li><p class="f-16">Developed by Association of Scouts of Azerbaijan</p></li>
                <li> <img style="height: 110px;" src="img/icons/logo2.png"/></li>
            </ul>
        </div>
    </div>
</footer>


<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1 class="c-white">Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="img/browsers/chrome.png" alt="">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="img/browsers/firefox.png" alt="">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="img/browsers/opera.png" alt="">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="img/browsers/safari.png" alt="">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="img/browsers/ie.png" alt="">
                    <div>IE (New)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->

<!-- Javascript Libraries -->
<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="vendors/bower_components/Waves/dist/waves.min.js"></script>

<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
<script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
<![endif]-->
<script src="js/app.min.js"></script>
<script src="js/constants.js"></script>
<script type="text/javascript">
    /*function login() {
     $.ajax({
     url : 'log-in',
     type : 'post',
     dataType : 'json',
     data : {
     username : $('[name="username"]').val(),
     password : $('[name="password"]').val()
     },
     success : function (data, textStatus, jqXHR) {
     console.log(data);
     console.log(textStatus);
     console.log(jqXHR);
     }
     })
     }*/
</script>
</body>
</html>
