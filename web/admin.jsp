<%@ page import="com.mv.domain.User" %>
<%@ page import="com.mv.utils.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User user = (User)session.getAttribute(Constants.LOGGED_USER);
    if (user == null){
        response.sendRedirect(Constants.PAGE_LOGIN);
    }
%>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>e-voting Admin</title>

    <!--<link rel="icon" href="img/icons/logo2.png">-->
    <link rel="shortcut icon" href="img/icons/favicon.ico"/>
    <!-- Vendor CSS -->
    <%--<link href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">--%>
    <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
    <link href="vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
    <link href="assert/icons/material-icons/material-icons.css" rel="stylesheet">
    <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
    <link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
    <!--<link href="vendors/bootstrap-multiselect/bootstrap-multiselect.css" rel="stylesheet">-->


    <!-- CSS -->


    <link href="css/app_1.min.css" rel="stylesheet">
    <link href="css/app_2.min.css" rel="stylesheet">
    <link href="css/media.css" rel="stylesheet">
    <link href="css/demo.css" rel="stylesheet">
    <link href="assert/css/color.css" rel="stylesheet">
    <link href="assert/css/fonts.css" rel="stylesheet">

    <link href="css/custom.css" rel="stylesheet">


    <link href="vendors/bower_components/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="vendors/bower_components/chosen/chosen.css" rel="stylesheet" type="text/css">


</head>
<body class="bg-grey-200">
<header id="header" class="clearfix" data-ma-theme="purple">
    <ul class="h-inner">
        <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="hidden-xs">
            <a href="#" class="m-l-10">
                <img style="height: 50px" src="img/icons/logo4.png" alt="">
            </a>
        </li>

        <li class="pull-right">
            <ul class="hi-menu">
                <li class="dropdown">
                    <a data-toggle="dropdown" href="admin">

                        <i new-connection class="him-counts him-new">+1</i>

                        <i class="him-icon material-icons">list</i>

                        <i all-connections class="bg-green him-counts him-all">0</i>

                    </a>
                    <div class="dropdown-menu pull-right dropdown-menu-lg">
                        <div surey_id="" id="current_survey_info" class="list-group">
                            <div class="lg-header">
                                Connected Users
                            </div>
                            <div id="connected-users-statistics" class="lg-body">
                                <div id="current_survey_info_name" class="text-center list-group-item p-0"></div>
                                <div class="list-group-item">
                                    <div class="lgi-heading m-b-5">Connected Cliets</div>

                                    <div id="client_progress" class="progress-wrapper">
                                        <div class="progress">
                                            <div class="progress-bar"  style="width: 0"> </div>
                                        </div>
                                        <div class="lgi-heading progress-stat"></div>
                                    </div>
                                    <a id="view-all-clients" class="view-more" href="javascript:void(0)">View All</a>
                                </div>

                                <div class="list-group-item">
                                    <div class="lgi-heading m-b-5">Connected Admins</div>

                                    <div id="admin_progress" class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="10" style="width: 50%">

                                        </div>
                                    </div>
                                    <div class="lgi-heading progress-stat"></div>
                                    <!--<a id="view-all-admins" class="view-more" href="javascript:void(0)">View All</a>-->
                                </div>
                            </div>
                            <div style="display: none;" id="connected-users-container" class="lg-body">
                                <div class="row">
                                    <div class="col-md-4"><a filter-conn-users-action="all" class="view-more" href="javascript:void(0)">All</a></div>
                                    <div class="col-md-4"><a filter-conn-users-action="online" class="view-more" href="javascript:void(0)">Online</a></div>
                                    <div class="col-md-4"><a filter-conn-users-action="offline" class="view-more" href="javascript:void(0)">Offline</a></div>
                                </div>
                                <div style="overflow-y: scroll; height: 250px;" id="connected-users"></div>
                                <a id="back-to-statistics" class="view-more" href="javascript:void(0)">Back</a>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="dropdown">
                    <a data-toggle="dropdown" href="admin"><i class="him-icon zmdi zmdi-more-vert"></i></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        <li class="skin-switch hidden-xs">
                            <span class="ss-skin bgm-lightblue" data-ma-action="change-skin" data-ma-skin="lightblue"></span>
                            <span class="ss-skin bgm-bluegray" data-ma-action="change-skin" data-ma-skin="bluegray"></span>
                            <span class="ss-skin bgm-cyan" data-ma-action="change-skin" data-ma-skin="cyan"></span>
                            <span class="ss-skin bgm-teal" data-ma-action="change-skin" data-ma-skin="teal"></span>
                            <span class="ss-skin bgm-orange" data-ma-action="change-skin" data-ma-skin="orange"></span>
                            <span class="ss-skin bgm-blue" data-ma-action="change-skin" data-sma-kin="blue"></span>
                        </li>
                        <li class="divider hidden-xs"></li>
                        <li class="hidden-xs">
                            <a data-ma-action="fullscreen" href="image-logo.html"><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
                        </li>
                        <li>
                            <a data-ma-action="clear-localstorage" href="image-logo.html"><i class="zmdi zmdi-delete"></i> Clear Local Storage</a>
                        </li>
                        <%--<li>
                            <a href="image-logo.html"><i class="zmdi zmdi-face"></i> Privacy Settings</a>
                        </li>
                        <li>
                            <a href="image-logo.html"><i class="zmdi zmdi-settings"></i> Other Settings</a>
                        </li>--%>
                    </ul>
                </li>

                <li>
                    <a class="text-right p-t-0 p-b-5 p-r-5 p-l-5" href="log-out"><i class="him-icon vertical-align-middle material-icons">exit_to_app</i><span class="f-16 m-l-5">Logout</span></a>
                </li>

            </ul>
        </li>
    </ul>

    <!-- Top Search Content -->
    <div class="h-search-wrap">
        <div class="hsw-inner">
            <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
            <input type="text">
        </div>
    </div>
</header>

<section id="operation-header" class="operation-header p-fixed bg-grey-200">
    <div class="operation-container">
        <ul class="list-style-none pull-left">
            <li id="module-title">

            </li>
        </ul>


        <ul class="pull-right">
            <ul id="operation-buttons" class="pull-left actions m-t-2">

            </ul>
            <ul class="pull-right actions">
                <li>
                    <a href="javascript:void(0)" data-toggle="dropdown">
                        <i class="material-icons">more_vert</i>
                    </a>
                </li>

                <li class="fi-trigger">
                    <a data-ma-action="filter-open" ma-action="filter-open" class="show-filter" href="javascript:void(0)">
                        <i class="material-icons f-35">filter_list</i>
                    </a>
                    <a data-ma-action="filter-close" ma-action="filter-close" class="hide-filter" href="javascript:void(0)">
                        <i class="material-icons f-35">arrow_forward</i>
                    </a>
                </li>
            </ul>
        </ul>


    </div>
</section>

<section data-filter="visible" id="main">
    <aside id="sidebar" class="sidebar c-overflow">
        <div class="s-profile">
            <a href="admin" data-ma-action="profile-menu-toggle">
                <div class="sp-pic">
                    <!--<img src="img/demo/profile-pics/1.jpg" alt="">-->
                </div>

                <div class="sp-info">
                    <%=user.getFirstName() + " " + user.getLastName()%>
                    <%--<i class="zmdi zmdi-caret-down"></i>--%>
                </div>
            </a>

            <%--<ul class="main-menu">
                <li>
                    <a href="javascript:void(0)"><i class="zmdi zmdi-account"></i> View Profile</a>
                </li>
                <li>
                    <a href="log-out"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                </li>
            </ul>--%>
        </div>
        <ul id="main-menu" class="main-menu">
            <li><a class="waves-effect waves-classic f-15" href="javascript:void(0);" module="countries"><i class="material-icons f-25 text-green-700">language</i>Countries</a></li>
            <li><a class="waves-effect waves-classic f-15" href="javascript:void(0);" module="surveys"><i class="material-icons f-25 text-green-700">subject</i>Surveys</a></li>
            <li><a class="waves-effect waves-classic f-15" href="javascript:void(0);" module="participants"><i class="material-icons f-25 text-green-700">supervisor_account</i>Participants</a></li>
            <li><a class="waves-effect waves-classic f-15" href="javascript:void(0);" module="questionnaires"><i class="material-icons f-25 text-green-700">live_help</i>Questionnaires</a></li>
            <li><a class="waves-effect waves-classic f-15" href="javascript:void(0);" module="reports"><i class="material-icons f-25 text-green-700">insert_chart</i>Reports</a></li>

            <li class="h-30"></li>

            <li><a class="waves-effect waves-classic f-15" href="javascript:void(0);" module="users"><i class="material-icons f-25 text-green-700">account_box</i>Administrators</a></li>
            <!--<li class="sub-menu">
                <a href="icons.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-view-list"></i> Tables</a>

                <ul>
                    <li><a href="#" link="tables.html">Normal Tables</a></li>
                    <li><a href="#" link="data-tables.html"> Data Tables</a></li>
                </ul>
            </li>


            <li class="sub-menu">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Dashboards</a>

                <ul>
                    <li><a href="#" link="dashboards/analytics.html">Analytics</a></li>
                    <li><a href="#"link="dashboards/school.html">School</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-swap-alt"></i>User Interface</a>
                <ul>
                    <li><a href="#" link="colors.html">Colors</a></li>
                    <li><a href="#" link="animations.html">Animations</a></li>
                    <li><a href="#" link="buttons.html">Buttons</a></li>
                    <li><a href="#" link="icons.html">Icons</a></li>
                    <li><a href="#" link="alerts.html">Alerts</a></li>
                    <li><a href="#" link="preloaders.html">Preloaders</a></li>
                    <li><a href="#" link="notification-dialog.html">Notifications & Dialogs</a></li>
                    <li><a href="#" link="media.html">Media</a></li>
                    <li><a href="#" link="components.html">Components</a></li>
                    <li><a href="#" link="other-components.html">Others</a></li>
                </ul>
            </li>

            <li class="sub-menu toggled active">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-view-compact"></i> Headers</a>

                <ul>
                    <li><a href="textual-menu.html">Textual menu</a></li>
                    <li><a class="active" href="image-logo.html">Image logo</a></li>
                    <li><a href="top-mainmenu.html">Mainmenu on top</a></li>
                </ul>
            </li>

            <li><a href="typography.html"><i class="zmdi zmdi-format-underlined"></i> Typography</a></li>
            <li class="sub-menu">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-widgets"></i> Widgets</a>

                <ul>
                    <li><a href="widget-templates.html">Templates</a></li>
                    <li><a href="widgets.html">Widgets</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-collection-text"></i> Forms</a>

                <ul>
                    <li><a href="form-elements.html">Basic Form Elements</a></li>
                    <li><a href="form-components.html">Form Components</a></li>
                    <li><a href="form-examples.html">Form Examples</a></li>
                    <li><a href="form-validations.html">Form Validation</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-trending-up"></i>Charts & Maps</a>
                <ul>
                    <li><a href="flot-charts.html">Flot Charts</a></li>
                    <li><a href="other-charts.html">Other Charts</a></li>
                    <li><a href="maps.html">Maps</a></li>
                </ul>
            </li>
            <li><a href="calendar.html"><i class="zmdi zmdi-calendar"></i> Calendar</a></li>
            <li class="sub-menu">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-image"></i>Photo Gallery</a>
                <ul>
                    <li><a href="photos.html">Default</a></li>
                    <li><a href="photo-timeline.html">Timeline</a></li>
                </ul>
            </li>

            <li><a href="generic-classes.html"><i class="zmdi zmdi-layers"></i> Generic Classes</a></li>
            <li class="sub-menu">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-collection-item"></i> Sample Pages</a>
                <ul>
                    <li><a href="profile-about.html">Profile</a></li>
                    <li><a href="list-report_view.html">List View</a></li>
                    <li><a href="messages.html">Messages</a></li>
                    <li><a href="pricing-table.html">Pricing Table</a></li>
                    <li><a href="contacts.html">Contacts</a></li>
                    <li><a href="wall.html">Wall</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="login.html">Login and Sign Up</a></li>
                    <li><a href="lockscreen.html">Lockscreen</a></li>
                    <li><a href="404.html">Error 404</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="image-logo.html" data-ma-action="submenu-toggle"><i class="zmdi zmdi-menu"></i> 3 Level Menu</a>

                <ul>
                    <li><a href="form-elements.html">Level 2 link</a></li>
                    <li><a href="form-components.html">Another level 2 Link</a></li>
                    <li class="sub-menu">
                        <a href="image-logo.html" data-ma-action="submenu-toggle">I have children too</a>

                        <ul>
                            <li><a href="image-logo.html">Level 3 link</a></li>
                            <li><a href="image-logo.html">Another Level 3 link</a></li>
                            <li><a href="image-logo.html">Third one</a></li>
                        </ul>
                    </li>
                    <li><a href="form-validations.html">One more 2</a></li>
                </ul>
            </li>-->
        </ul>
    </aside>

    <section id="filter" class="filter p-fixed p-t-20 p-b-20 p-r-15 p-l-15">

    </section>

    <section id="content">
        <div id="container" class="container">

        </div>
    </section>
</section>

<footer style="color: black;" id="footer" class="p-fixed bg-grey-200 p-t-5 text-green-900">
    <span class="pull-left">Developed by Association of Scouts of Azerbaijan 2017</span>
    <span class="pull-right">
        Copyright &copy; 2017 Scouts of Azerbaijan
        <%--<a class="text-purple-900" href="about">About</a>--%>
    </span>
</footer>

<!-- Page Loader -->
<div class="page-loader">
    <div class="preloader pls-blue">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p>Please wait...</p>
    </div>
</div>

<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1 class="c-white">Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="img/browsers/chrome.png" alt="">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="img/browsers/firefox.png" alt="">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="img/browsers/opera.png" alt="">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="img/browsers/safari.png" alt="">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="img/browsers/ie.png" alt="">
                    <div>IE (New)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->




<!-- Javascript Libraries -->
<script type="text/javascript" src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!--<script src="vendors/bower_components/flot/jquery.flot.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
<script src="vendors/sparklines/jquery.sparkline.min.js"></script>-->
<!--<script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>-->

<script type="text/javascript" src="vendors/bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<!--<script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>-->
<script type="text/javascript"  src="vendors/bower_components/Waves/dist/waves.min.js"></script>
<script type="text/javascript"  src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script type="text/javascript"  src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
<script type="text/javascript"  src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript"  src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>

<script type="text/javascript"  src="vendors/bower_components/chosen/chosen.jquery.js"></script>

<script type="text/javascript"  src="vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<!----- type="text/javascript" --------------inputmask---------------------->
<script type="text/javascript"  src="vendors/bower_components/inputmask/dist/jquery.inputmask.bundle.js"></script>
<script type="text/javascript"  src="vendors/bower_components/inputmask/dist/min/inputmask/phone-codes/phone.min.js"></script>
<script type="text/javascript"  src="vendors/bower_components/inputmask/dist/min/inputmask/bindings/inputmask.binding.min.js"></script>

<!--<script src="vendors/bower_components/inputmask/dist/min/inputmask/phone-codes/phone-be.js"></script>-->
<!--<script src="vendors/bower_components/inputmask/dist/min/inputmask/phone-codes/phone-ru.js"></script>-->

<!-------------------inputmask---------------------->
<script type="text/javascript">



    var oldSize = $(window).width();
    $(window).resize(function () {

        if( $(window).width() < oldSize){
            if (window.matchMedia('(max-width: 1279px)').matches){
                $("body").addClass("filter-resize-toggled");
            }
        }
        oldSize = $(window).width();
    });




    $(function () {
        if (window.matchMedia('(max-width: 1279px)').matches){
            $("body").addClass("filter-resize-toggled");
        }


    });
</script>

<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
<script type="text/javascript" src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
<![endif]-->

<script type="text/javascript" src="js/app.min.js"></script>
<script type="text/javascript" src="js/constants.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/modules.js"></script>
<script type="text/javascript" src="js/plugins.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<!--<script src="vendors/bootstrap-multiselect/bootstrap-multiselect.min.js"></script>-->

<script type="text/javascript" src="js/survey.js"></script>
<script type="text/javascript" src="js/questionnaire.js"></script>
<script type="text/javascript" src="js/socket.js"></script>
<script type="text/javascript" src="js/client.js"></script>
<script type="text/javascript" src="js/users.js"></script>



<script type="text/javascript" type="text/javascript">
    function initStatistics() {


        $().getComboContent('cs?action=survey&sub_action=getCurrentSurveyDetails', {}, function (d) {
            console.log('getCurrentSurveyDetails..');

            if(d.resultMessage != undefined){
                if (d.resultMessage.resultStatus == 'ERROR' ){
                    $("#current_survey_info_name").html('There is no active survey');
                } else {
                    $("#current_survey_info_name").html('There is no active survey');
                }
            }else {
                var surveyInfo = d[0].data[0];
                surveyInfo.countries = d[1].data;

                var current_survey_info = $("#current_survey_info_name");
                current_survey_info.html(surveyInfo.name);
                $("#current_survey_info").data("survey_info", surveyInfo);


                $("#client_progress").progressBar('init', {
                    'max-value' : surveyInfo.countries.length
                });

                $.socket('send', 'GET_CLIENTS', { });
            }


        });
    }

    $(function () {
        var sendedQuestionnaire = undefined;
        getWebSocketUrl(function (data) {
            $.socket('open', {
                'end-point': data.data + '/admin',
                'events' : {
                    'on-open' : function () {
                        console.log("ws-on-open");
                    },
                    'no-close' : function () {

                    },
                    'on-message' : function (e,action, data) {
                        console.log("--------------- on-message -data ---------------");
                        console.log(data);

                        var actions = {
                            "ADMIN_LOGIN" : function (data) {
                                $('i[new-connection]').removeClass('bgm-red').html('+1');
                                $('i[all-connections]').html(data.body[1]);
                            },
                            "CLIENT_LOGIN" : function (data) {
                                $('i[new-connection]').addClass('bgm-red').html('+1');
                                $('i[all-connections]').html(data.body);
                                $("#client_progress").progressBar('update', data.body);
                            },
                            "ADMIN_CLOSE" : function (data) {
                                $('i[new-connection]').removeClass('bgm-red').html('-1');
                            },
                            "CLIENT_CLOSE" : function (data) {
                                $('i[new-connection]').addClass('bgm-red').html('-1');
                                $('i[all-connections]').html(data.body[1]);
                            },
                            /*"GET_ADMINS" : function (data) {
                             $("#client_progress").progressBar('update', data.body.length);
                             $.each(data.body, function (i, d) {
                             $("#connected-users").find('[country-id="'+ d.id +'"]').find('i[status-icon]').addClass('status-online');
                             $("#connected-users").find('[country-id="'+ d.countryId +'"]').attr('status', 'offline');
                             });
                             },*/
                            "BROADCAST_QUESTION" : function (data) {
                                sendedQuestionnaire = data.body;
                                $.closeNotify();
                            },
                            "BROADCAST_TIME" : function (data) {
                                var tt = data.body;
                                //var target = $('[questionnaire-timer="'+ sendedQuestionnaire.questionnaireMovementId +'"]');
                                if (tt > 0){
                                    $('#timer-countdown-icon').html('timer');
                                } else {
                                    $('#timer-countdown-icon').html('timer_off');
                                }
                                $('#timer-countdown').html(tt.toString().toTimeFormat());
                            },
                            "GET_CLIENTS" : function (data) {
                                $("#client_progress").progressBar('update', data.body.length);
                                var connected_users = $("#connected-users");
                                $.each(data.body, function (i, d) {
                                    connected_users.find('[country-id="'+ d.surveyMember.country.id +'"]').find('i[status-icon]').removeClass('status-offline').addClass('status-online');
                                    connected_users.find('[country-id="'+ d.surveyMember.country.id +'"]').attr('status', 'online');
                                });
                            }
                        };
                        actions[action](data);
                    }
                }
            });


            initStatistics();
        });

        $("#view-all-clients").click(function () {
            var surveyInfo = $("#current_survey_info").data("survey_info");
            if (surveyInfo != undefined){
                var x = $("#connected-users-statistics");
                x.hide();
                $("#connected-users-container").show();
                var connected_users = $("#connected-users");
                var p = connected_users.preloader('append', {size:2});
                //$().getComboContent('cs?action=common&sub_action=getDataContent&type=getCountries', {}, function (d) {
                var current_survey_info = $("#current_survey_info");

                var data = current_survey_info.data('survey_info');
                var html = '';
                $.each(data.countries, function (i, c) {
                    html += '<a status="offline" country-id="'+ c.country_id +'" class="list-group-item media" href="javascript:void(0)">' +
                        '<div class="pull-left">' +
                        '<img class="lgi-img" src="'+ c.icon +'" alt="">' +
                        '<i status-icon class="status status-offline"></i>' +
                        '</div>' +
                        '<div class="media-body">' +
                        '<div class="lgi-heading">'+ c.country_name +'</div>' +
                        //'<small class="lgi-text">Cum sociis natoque penatibus et magnis dis parturient montes</small>' +
                        '</div>' +
                        '</a>'
                });

                $("#connected-users").html(html);
                $.socket('send', { action : 'GET_CLIENTS', body : {}});
                p.remove();
                //});
            } else {
                $.notify1('There is no an active survey', messagesTypes.ERROR);
            }
        });

        $("#view-all-admins").click(function () {
//                    $.socket('send', {
//                        'action' : $.socket('actions').GET_CLINTS
//                    });
        });

        $("#back-to-statistics").click(function () {
            var x = $("#connected-users-statistics");
            x.show();
            var ss = $("#connected-users-container");
            ss.hide();
            $("#connected-users").html('')
        });

        $("[filter-conn-users-action]").click(function () {
            var ac = $(this).attr("filter-conn-users-action");
            if (ac == 'all'){
                $("#connected-users").find('[country-id]').show();
            } else {
                $("#connected-users").find('[country-id]').hide();
                $("#connected-users").find('[country-id][status="'+ ac +'"]').show();
            }
        })
    });
</script>
</body>
</html>

<div id="modal-dialog" class="modal fade" role="dialog"></div>
