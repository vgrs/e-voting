jQuery.fn.getComboContent = function (url, data, callback){

    var combo = $(this);

    combo.empty();

    var settings = $.extend({
        comboId     : 'id',
        comboName   : 'name',
        data_array  : 'data'
    }, {
        comboId     :combo.attr("comboId"),
        comboName   :combo.attr("comboName")
    });

    var x = [];
    var d = [];
    if ( combo.hasClass('selectpicker') ){
        combo.selectpicker('refresh');
        combo.next('.bootstrap-select').find('.dropdown-toggle').css('cursor','progress');
        /*combo.next('.bootstrap-select').find('.filter-option').prepend( $().loading('return'));*/
    } else {
        combo.css('cursor','progress');
    }
    if (data != undefined){
        data["current_timestamp_for_cache"] = new Date().getTime();
    }
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: url,
        data: data,
        success: function (result){
            x = result[settings.data_array];
            /*d = result[settings.additional_data_array];*/
            if ( x != undefined ){
                if ( combo.length > 0 ){
                    $.each(x, function (i, d) {
                        var e = $(document.createElement('option'));
                        e.text(d[settings.comboName]);
                        e.val(d[settings.comboId]);
                        $.each(d, function (j, k) {
                            if (j != settings.comboName && j != settings.comboId ) {
                                e.attr(j, k);
                            }
                        });
                        combo.append(e);
                    });
                }
            }

            if (callback) {
                console.log(result);
                callback(result /*, d*/);
            }
        },
        complete: function () {
            if ( combo.hasClass('selectpicker') ){
                var bootstrap =  combo.next('.bootstrap-select');

                bootstrap.find('.dropdown-toggle').css('cursor','');

                combo.selectpicker('refresh');
            } else {
                combo.css('cursor','');
            }
        }
    });

    return combo.find('option').length;
};

$.fn.userDefinedModal = function(method, options) {
    var th = $(this);

    var events = $.extend({
        'on-hide' : function () {
            th.empty();

        },
        'on-open' : function (data) {

        }
    }, options == undefined ? {} : options.events);

    var methods = {
        'open' : function (op) {
            if(op){
                if (op.buttons){
                    $.each(Object.keys(op.buttons), function (index, value ) {
                        $(value).on('click', function () {
                            op.buttons[value]();
                        })
                    });

                    th.on('hidden.bs.modal', function (e) {
                        events['on-hide']();
                        $(this).data('bs.modal', null);
                    });

                    th.on('show.bs.modal', function (e) {
                        events['on-open']()
                    });
                }
                th.modal('show');
            }
        },
        'close':function (){
            th.empty();
            th.modal('hide');
        }
    };

    if (method){
        methods[method](options)
    }else{
        methods['open'](options)
    }
};

$.fn.preloader = function (method , data) {
    var th = $(this);
    var size = ['pl-xs', 'pl-sm', 'pl-lg', 'pl-xl', 'pl-xxl'];

    var id = th.attr("id") != undefined ? th.attr("id")  + '_preloader' : new Date().getDate() + '_preloader' ;


    var methods = {
        'append' : function (op) {
            var s = '';
            if (op == undefined) {s = ''} else if ( op.size == undefined) {size = '' }else { s = size[op.size] } ;
            if (th.find('[preloader]').length == 0){
                var html = $('<div preloader class="preloader '+ s +'">' +
                    '<svg class="pl-circular" viewBox="25 25 50 50">' +
                    '<circle class="plc-path" cx="50" cy="50" r="20"></circle>' +
                    '</svg></div>');
                th.append(html);
                return html;
            } else{
                return undefined;
            }
        },
        'clear' : function (id) {
            th.find('[preloader]').remove();
        }
    };

    if ( !methods[method] ){
        return methods['append'](data);
    }else {
        return methods[method](data);
    }
};


$.fn.progressBar = function (method , data) {
    var th = $(this);

    var opt = $.extend({
        'min-value' : 0,
        'max-value' : 100,
        'curr-value' : 0

    }, data == undefined ? {} : data);

    var methods = {
        'init' : function (data) {
            th.find('.progress-bar').attr('aria-value-now', '0');
            th.find('.progress-bar').attr('aria-value-min', opt['min-value']);
            th.find('.progress-bar').attr('aria-value-max', opt['max-value']);

            /*console.log(opt['curr-value'] + ' connected of ' + opt['max-value']);
            console.log(th.find('.progress-stat'));*/
            th.find('.progress-stat').html(opt['curr-value'] + ' connected of ' + opt['max-value']);

            var x = (parseInt(opt['curr-value']) * 100) / (parseInt(opt['max-value']) - parseInt(opt['min-value']) );

            th.find('.progress-bar').css('width', x);
        },
        'update' : function (value) {
            var max = th.find('.progress-bar').attr('aria-value-max');
            var min = th.find('.progress-bar').attr('aria-value-min');
            var x = (parseInt(value) * 100) / (parseInt(max) - parseInt(min));
            th.find('.progress-bar').attr('aria-value-now', value);
            th.find('.progress-bar').css('width', x);
            th.find('.progress-stat').html(value + ' connected is ' + max);
        }
    };

    if ( methods[method] ){
        return methods[method](data);
    }else {
        return methods['init'](data);
    }
};



