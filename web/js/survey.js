function addSurvey(survey, callback) {
    $.ajax({
        type : 'POST',
        url : 'cs?action=survey&sub_action=addSurvey',
        dataType : 'json',
        data : {survey : JSON.stringify(survey)},
        success : function(data){
            if (callback){
                callback(data)
            }
        }
    })
}

function editSurvey(survey, callback) {
    $.ajax({
        type : 'POST',
        url : 'cs?action=survey&sub_action=editSurvey',
        dataType : 'json',
        data : {survey : JSON.stringify(survey)},
        success : function(data){
            if(callback){
                callback(data);
            }
        }
    });
}

function surveyModalNew(modal) {
    modal.userDefinedModal('open', {
        events : {
            'on-open': function () {

                /*---------------dataTable destroy--------------------*/
                if($('#data-table-survey-members').DataTable()) {
                    $('#data-table-survey-members').DataTable().destroy();
                }

                /*-----------create dataTable--------------*/
                var data_table_survey_members = $("#data-table-survey-members").DataTable({
                    "searching": false,
                    "ordering":  false,
                    "paging": false,
                    "info": false,
                    "columns" : [
                         { "name": "#", "render" : retDataTableRowIndex, "visible" : true},
                        { "name": "id", "data": "id", "visible" : false},
                        { "name": "user_id", "data": "user_id", "visible" : false},
                        { "name": "name", "data": "name"},
                        { "name": "username", "render": function ( data, type, full, meta ) {
                            return '<input class="none p-5 h-40 w-100" survey-member-username type="text" placeholder="Type username here..."/>';
                        }
                        },
                        { "name": "operations", "render": function ( data, type, full, meta ) {
                            return '<a class="btn btn-default" href="javascript:void(0)" onclick="survey_members_delete_from_table(this)"><i class="zmdi zmdi-minus"></i></a>';
                        }
                        }
                    ]
                });
                /*-----------------country list----------------*/
                var members_combo = $("#survey-members-combo");
                members_combo.selectpicker('refresh');
                members_combo.getComboContent("cs?action=common&sub_action=getDataContent&type=getCountries");
                members_combo.on('hidden.bs.select', function (e) {

                });

            }
        },

        buttons: {
            '#survey_operation' : function () {
                var members = $('#data-table-survey-members');
                var survey_name = $("#survey-name");
                var tt = true;
                members.find('[survey-member-username]').each(function (element) {
;                    if ($(this).val().trim().length == 0){
                        tt = false;
                        $(this).addClass('brd-red-600i');
                    }
                });
                if ((survey_name.val().trim().length == 0) || !tt){
                    survey_name.addClass("brd-red-600");
                    $.notify1('Please enter important fields', messagesTypes.ERROR);
                } else {

                    var precont = modal.find('[preloader-container]');
                    var p = precont.preloader('append', {size: 1});

                    var surveyMembers = [];
                    members.DataTable().rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                        var row = members.find('tbody').find('tr[role="row"]')[rowIdx];
                        var inp = $(row).find('input.none');
                        var inpVal = $(inp).val().toString().trim();

                        surveyMembers.push({
                            id: 0,
                            survey: {},
                            userId: 0,
                            country: {id: this.data().id, name: '', flag: ''},
                            username: inpVal != '' ? inpVal : this.data().name
                        });
                    });
                    addSurvey({
                        id: 0,
                        name: survey_name.val(),
                        surveyMembers: surveyMembers
                    }, function (data) {
                        $.notify2(data);
                        p.remove();
                        modal.userDefinedModal('close');
                        modules.getCurrent().data_table.ajax.reload();
                    });

                }
            }
        }
    });
}

function surveyModalEdit(modal, surveyData) {
    modal.userDefinedModal('open', {
        events : {
            'on-open': function () {

                /*---------------dataTable destroy--------------------*/
                if($('#data-table-survey-members').DataTable()) {
                    $('#data-table-survey-members').DataTable().destroy();
                }

                /*-----------create dataTable--------------*/
                var data_table_survey_members = $("#data-table-survey-members").DataTable({
                    "searching": false,
                    "ordering":  false,
                    "paging": false,
                    "info": false,
                    "columns" : [
                        { "name": "#", "render" : retDataTableRowIndex, "visible" : true},
                        { "name": "id", "data": "id", "visible" : false},
                        { "name": "user_id", "data": "user_id", "visible" : false},
                        { "name": "name", "data": "name"},
                        { "name": "username", "render": function ( data, type, full, meta ) {
                            var username = full.username == undefined  ? '' : full.username;
                            return '<input class="none p-5 h-40 w-100" survey-member-username type="text" value="' + username + '" placeholder="Type username here..."/>';
                        }
                        },
                        { "name": "operations", "render": function ( data, type, full, meta ) {
                            return '<a class="btn btn-default" href="javascript:void(0)" onclick="survey_members_delete_from_table(this)"><i class="zmdi zmdi-minus"></i></a>';
                        }
                        }
                    ]
                });


                $().getComboContent('cs?action=survey&sub_action=getSurveyDetails', {
                    survey_id :surveyData.id
                }, function (data) {
                    console.log(data);

                    /*-----------------country list----------------*/
                    var members_combo = $("#survey-members-combo");
                    members_combo.getComboContent("cs?action=common&sub_action=getDataContent&type=getCountries", {}, function () {
                        var surveyDetails = data[0].data[0];
                        $("#survey-name").val(surveyDetails.name);

                        var surveyMembers = data[1].data;
                        $.each(surveyMembers, function (i, member) {
                            data_table_survey_members.row.add({
                                "id"        : member.country_id,
                                "user_id"   : member.user_id,
                                "name"      : member.country_name,
                                "username"  : member.username
                            }).draw(false);

                            members_combo.find('option[value='+ member.country_id +']').removeAttr('selected').hide();

                        });
                        $("#summary-count-of-members").html("Total : " + data_table_survey_members.rows().data().length + " members");
                        members_combo.selectpicker('refresh');
                    });
                });
            }
        },

        buttons: {
            '#survey_operation' : function () {

                var members = $('#data-table-survey-members');
                var survey_name = $("#survey-name");
                var tt = true;
                members.find('[survey-member-username]').each(function (element) {
                    if ($(this).val().trim().length == 0){
                        tt = false;
                        $(this).addClass('brd-red-600i');
                    }
                });

                if ((survey_name.val().trim().length == 0) || !tt) {
                    survey_name.addClass("brd-red-600");
                    $.notify1('Please enter important fields', messagesTypes.ERROR);

                } else {
                    var precont = modal.find('[preloader-container]');
                    var p = precont.preloader('append', {size: 1});

                    var surveyMembers = [];
                    members.DataTable().rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                        var row = members.find('tbody').find('tr[role="row"]')[rowIdx];
                        var inp = $(row).find('input.none');
                        var inpVal = $(inp).val().toString().trim();

                        surveyMembers.push({
                            id: 0,
                            survey: {},
                            userId: this.data().user_id,
                            country: {id: this.data().id, name: '', flag: ''},
                            username: inpVal != '' ? inpVal : this.data().name
                        });
                    });

                    editSurvey({
                        id: surveyData.id,
                        name: survey_name.val(),
                        surveyMembers: surveyMembers
                    }, function (data) {
                        $.notify2(data);
                        p.remove();
                        modal.userDefinedModal('close');
                        modules.getCurrent().data_table.ajax.reload();
                    })
                }
            }
        }
    });
}

/*-----------------------------remove from dataTable add to combo box----------------------------------*/
function survey_members_delete_from_table(a){

    var table = $('#data-table-survey-members').DataTable();

    var dataValue = table.row($(a).parents('tr[role="row"]')).data().id;

    // $('li[data-value="' + dataValue + '"]').attr('style', '');

    table.row($(a).parents('tr[role="row"]')).remove().draw();


    $('#survey-members-combo').find('option[value="'+dataValue+'"]').attr('data-repeat', '');//allows add second time same data
    $('#survey-members-combo').find('option[value="'+dataValue+'"]').show();
    $('#survey-members-combo').selectpicker('refresh');


    $("#summary-count-of-members").html("Total : " + table.rows().data().length + " members");
}

/*-----------------------------------insert to dataTable-------------------------------------*/
function survey_members_add_2_table(a){

    var survey_combo = $('#survey-members-combo');

    if( survey_combo.find('option:selected').attr('data-repeat') != 'repeat' &&  survey_combo.find('option:selected').val() !=0 ) {
        var table = $('#data-table-survey-members').DataTable();
        var selOption = survey_combo.find('option:selected');
        table.row.add({
            "id": selOption.val(),
            "user_id" : 0,
            "name": selOption.text()
        }).draw(false);

        selOption.removeAttr('selected').hide();

        survey_combo.selectpicker('refresh');

        survey_combo.find('option:selected').attr('data-repeat', 'repeat');//block repeated data in datatable

        $("#summary-count-of-members").html("Total : " + table.rows().data().length + " members");

    }


}