/*
$.socket = function (method , options){

    var Constants = {
        endPoint : undefined,
        webSocket : undefined,
        token : undefined
    };

    var methods = {
        'open' : function (op) {
            Constants.endPoint = op['end-point'];
            if ( Constants.endPoint != undefined ){
                if(Constants.webSocket !== undefined && socket.webSocket.readyState !== WebSocket.CLOSED){
                    //writeResponse("WebSocket is already opened.");
                    return;
                }
                // http://localhost:8064/agr/
                //chatModel.webSocket = new WebSocket( chatModel.endPoint );


                if ('WebSocket' in window) {
                    Constants.webSocket = new WebSocket( Constants.endPoint );
                } else if ('MozWebSocket' in window) {
                    socket.webSocket = new MozWebSocket( Constants.endPoint );
                } else {
                    console.log('Error: WebSocket is not supported by this browser.');
                    return false;
                }


                Constants.webSocket.onopen = function(event){
                    events['on-open'](event);
                };

                Constants.webSocket.onmessage = function(event){

                    var incomingMessage = JSON.parse( event.data );

                    Constants.token = incomingMessage.header.token;

                    events['on-message'](event, incomingMessage);

                };

                Constants.webSocket.onclose = function(event){
                    events['on-close'](event);
                };

            }
        },
        'send' : function (data) {
            /!*Constants.webSocket.send( JSON.stringify({
                message : {
                    action : 'message',
                    userId : parseInt(user_id),
                    text : text
                }
            }));*!/
            Constants.webSocket.send( JSON.stringify({ header : {token : Constants.token}, body : data }));
        },
        'actions' : function () {
            return {
                ADMIN_LOGIN : "ADMIN_LOGIN",
                CLIENT_LOGIN : "CLIENT_LOGIN",
                ADMIN_CLOSE : "ADMIN_CLOSE",
                CLIENT_CLOSE : "CLIENT_CLOSE",
                GET_ADMINS : "GET_ADMINS",
                GET_CLIENTS : "GET_CLIENTS"
            }
        }
    };

    var events = $.extend({
        'on-open' : function (event) {

        },
        'on-message' : function (event, data) {

        },
        'on-close' : function (event) {

        }
    }, options == undefined ? {} : options.events);

    methods[method](options);

};*/
(function($){
    var SocketConstants = {
        endPoint : undefined,
        webSocket : undefined,
        token : undefined,
        ACTION_DELIMITER : '.',
        ACTIONS : {
            ADMIN_LOGIN : "ADMIN_LOGIN",
            CLIENT_LOGIN : "CLIENT_LOGIN",
            ADMIN_CLOSE : "ADMIN_CLOSE",
            CLIENT_CLOSE : "CLIENT_CLOSE",
            GET_ADMINS : "GET_ADMINS",
            GET_CLIENTS : "GET_CLIENTS",
            BROADCAST_QUESTION : "BROADCAST_QUESTION"
        }
    };

    $.socket = function (method , options){

        var methods = {
            'open' : function (op) {
                SocketConstants.endPoint = op['end-point'];
                if ( SocketConstants.endPoint != undefined ){
                    if(SocketConstants.webSocket !== undefined && socket.webSocket.readyState !== WebSocket.CLOSED){
                        //writeResponse("WebSocket is already opened.");
                        return;
                    }

                    if ('WebSocket' in window) {
                        SocketConstants.webSocket = new WebSocket( SocketConstants.endPoint );
                    } else if ('MozWebSocket' in window) {
                        socket.webSocket = new MozWebSocket( SocketConstants.endPoint );
                    } else {
                        console.log('Error: WebSocket is not supported by this browser.');
                        return false;
                    }


                    SocketConstants.webSocket.onopen = function(event){
                        events['on-open'](event);
                    };

                    SocketConstants.webSocket.onmessage = function(event){
                        if (event.data){
                            var ind = event.data.indexOf(SocketConstants.ACTION_DELIMITER);
                            if (ind != -1){
                                var incomingMessage;
                                var action = event.data.substr(0, ind);
                                try{
                                    incomingMessage = {action : action, body : JSON.parse( event.data.substr(ind + 1) )};
                                }catch (e){
                                    incomingMessage = {action : action, body : event.data.substr(ind + 1)};
                                }
                                events['on-message'](event, event.data.substr(0, ind) , incomingMessage);
                            }
                        }
                    };

                    SocketConstants.webSocket.onclose = function(event){
                        events['on-close'](event);
                    };

                }
            },
            'send' : function (op) {
                SocketConstants.webSocket.send(op.action + SocketConstants.ACTION_DELIMITER + JSON.stringify(op.body));
            }
        };

        var events = $.extend({
            'on-open' : function (event) {

            },
            'on-message' : function (event, action , data) {

            },
            'on-close' : function (event) {

            }
        }, options == undefined ? {} : options.events);

        methods[method](options);

    };

})(jQuery);