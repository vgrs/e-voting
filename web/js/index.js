function toggleFilter(th){

    var content = $("#main");
    var target = $(th);
    var action_attr_name = 'v-action';
    var action = target.attr(action_attr_name);

    var methods = {
        'hide' : function () {
            content.attr("data-filter", "hidden");
            target.attr(action_attr_name, 'show');
        },
        'show' : function () {
            content.attr("data-filter", "visible");
            target.attr(action_attr_name, 'hide');
        }
    };

    console.log($(th));
    if (action != undefined){
        methods[action]();
    }else{
        console.log(action);
    }

}


function getToken(callback) {
    $.ajax({
        url : 'get-token',
        dataType : 'json',
        success : function (data) {
            if (callback)
                callback(data)
        }
    });
}

function getWebSocketUrl(callback) {
    $.ajax({
        url : 'cs?action=utils&sub_action=getWebSocketUrl',
        dataType : 'json',
        success : function (data) {
            if (callback)
                callback(data)
        }
    });
}