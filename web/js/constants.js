var Constants = {
    MODULE_MENU_CONTAINER : '#main-menu',
    MODULE_PAGE_CONTAINER : '#container',
    MODULE_FILTER_CONTAINER : '#filter',
    MODULE_TITLE_CONTAINER : '#module-title',
    MODULE_OPEARTION_CONTAINER : '#operation-buttons',

    PAGE_ADMIN : 'admnin',
    PAGE_CLIENT : 'client',
    PAGE_LOGIN : 'login',
    PAGE_REPORT : 'report'
};