package com.mv.dao.postgres;

import com.mv.dao.BaseDao;
import com.mv.dao.SurveyDao;
import com.mv.domain.*;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Converter;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import static com.mv.utils.PasswordGenerator.getUserPassword;
import static com.mv.utils.Utils.toSHA1;

public class SurveyDaoPostgresImpl extends BaseDao implements SurveyDao {

    private static final Logger logger = FactoryLogger.getLogger();

    public SurveyDaoPostgresImpl(User loggedUser){super(loggedUser);}

    @Override
    public DataContent getSurvey() throws UserDefinedException{
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.get_survey()}");
            connector.callableStatement.registerOutParameter(1, Types.OTHER);
            connector.callableStatement.execute();
            connector.resultSet = (ResultSet) connector.callableStatement.getObject(1);
            return connector.readCursor();
        }catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Long addSurvey(Survey survey) throws UserDefinedException{
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            //Survey survey1 = new Survey();
            List<SurveyMember> surveyMembers = new ArrayList<>();
            for(int i = 0; i < survey.getSurveyMembers().size(); i++){
                SurveyMember member = new SurveyMember();
                Country country = new Country();
                country.setId(survey.getSurveyMembers().get(i).getCountry().getId());
                member.setCountry(country);
                member.setUsername(survey.getSurveyMembers().get(i).getUsername());
                String s = getUserPassword();
                member.setPasswordOriginal(s);
                member.setPassword(toSHA1(s.getBytes()));
                surveyMembers.add(member);
            }
//            Array arr = connector.connection.createArrayOf("text", Converter.toPostgresSurveyMembers(survey.getSurveyMembers()));
            Array arr = connector.connection.createArrayOf("text", Converter.toPostgresSurveyMembers(surveyMembers));
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.add_survey(?,?,?)}");
            connector.callableStatement.registerOutParameter(1, Types.NUMERIC);
            connector.callableStatement.setString(2, survey.getName());
            connector.callableStatement.setInt(3, getLoggedUser().getUserId());
            connector.callableStatement.setArray(4,  arr);
            connector.callableStatement.execute();
            connector.connection.commit();
            return ((BigDecimal) connector.callableStatement.getObject(1)).longValue();
        }catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Boolean editSurvey(Survey survey) throws UserDefinedException {



        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();

            for( SurveyMember member : survey.getSurveyMembers() ){
                String s = getUserPassword();
                member.setPasswordOriginal(s);
                member.setPassword(toSHA1(s.getBytes()));
            }
//            Array arr = connector.connection.createArrayOf("text", Converter.toPostgresSurveyMembers(survey.getSurveyMembers()));
            Array arr = connector.connection.createArrayOf("text", Converter.toPostgresSurveyMembers(survey.getSurveyMembers()));

//            Array arr = connector.connection.createArrayOf("text", Converter.toPostgresSurveyMembers(survey.getSurveyMembers()));
            connector.callableStatement = connector.connection.prepareCall("{call evoting.edit_survey(?,?,?,?)}");
            connector.callableStatement.setLong(1, survey.getId());
            connector.callableStatement.setString(2, survey.getName());
            connector.callableStatement.setInt(3, getLoggedUser().getUserId() );
            connector.callableStatement.setArray(4,  arr);
//            connector.callableStatement.setArray(4,  arr);
            connector.callableStatement.execute();
            connector.connection.commit();
            return true;
        }catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Boolean deleteSurvey(long id) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{call evoting.delete_survey(?)}");
            connector.callableStatement.setLong(1, id);
            connector.callableStatement.execute();
            connector.connection.commit();
            return true;
        }catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Boolean stopSurvey(long id) throws UserDefinedException {

        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()) {
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{call evoting.stop_survey(?)}");
            connector.callableStatement.setLong(1, id);
            connector.callableStatement.execute();
            connector.connection.commit();
            return true;
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }

    }

    @Override
    public DataContent[] getSurveyInfo(long id) throws UserDefinedException {
        try(DBConnectorPostgresSQL c = DBConnectorPostgresSQL.getInstance()) {
            DataContent[] dataContents = new DataContent[2];
            c.openConnection();
            c.callableStatement = c.connection.prepareCall("{call evoting.get_suvey_details(?,?,?)}");
            c.callableStatement.setLong(1, id);
            c.callableStatement.registerOutParameter(2, Types.OTHER);
            c.callableStatement.registerOutParameter(3, Types.OTHER);
            c.callableStatement.execute();

            c.resultSet = (ResultSet)c.callableStatement.getObject(2);
            dataContents[0] = c.readCursor();
            c.closeResultSet();
            c.resultSet = (ResultSet)c.callableStatement.getObject(3);
            dataContents[1] = c.readCursor();

            return dataContents;
        } catch (UserDefinedException e) {
            logger.error(e);
            throw e;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }


    @Override
    public DataContent[] getCurrentSurveyInfo() throws UserDefinedException {
        try(DBConnectorPostgresSQL c = DBConnectorPostgresSQL.getInstance()) {
            DataContent[] dataContents = new DataContent[2];
            c.openConnection();
            c.callableStatement = c.connection.prepareCall("{call evoting.get_current_suvey_details(?,?)}");
            c.callableStatement.registerOutParameter(1, Types.OTHER);
            c.callableStatement.registerOutParameter(2, Types.OTHER);
            c.callableStatement.execute();

            c.resultSet = (ResultSet)c.callableStatement.getObject(1);
            dataContents[0] = c.readCursor();
            c.closeResultSet();
            c.resultSet = (ResultSet)c.callableStatement.getObject(2);
            dataContents[1] = c.readCursor();

            return dataContents;
        } catch (UserDefinedException e) {
            logger.error(e);
            throw e;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }
}
