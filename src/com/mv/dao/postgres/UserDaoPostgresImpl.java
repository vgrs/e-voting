package com.mv.dao.postgres;

import com.mv.dao.BaseDao;
import com.mv.dao.UserDao;
import com.mv.domain.DataContent;
import com.mv.domain.User;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import static com.mv.utils.PasswordGenerator.getUserPassword;
import static com.mv.utils.Utils.toSHA1;

public class UserDaoPostgresImpl extends BaseDao implements UserDao{

    private static final Logger logger = FactoryLogger.getLogger();

    public UserDaoPostgresImpl(User loggedUser) {
        super(loggedUser);
    }

    @Override
    public DataContent getUsers(Integer surveyId, Integer userType, String search) throws UserDefinedException {

        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
                connector.openConnection();
                connector.callableStatement = connector.connection.prepareCall("{? = call evoting.get_users(?,?,?)}");
                connector.callableStatement.registerOutParameter(1, Types.OTHER);
                connector.callableStatement.setInt(2, surveyId);
                connector.callableStatement.setInt(3, userType);
                connector.callableStatement.setString(4, search);
                connector.callableStatement.execute();
                connector.resultSet = (ResultSet)connector.callableStatement.getObject(1);

                return connector.readCursor();

        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public DataContent getParticipantDetails(long userId) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.get_participant_details(?)}");
            connector.callableStatement.registerOutParameter(1, Types.OTHER);
            connector.callableStatement.setLong(2, userId);
            connector.callableStatement.execute();
            connector.resultSet = (ResultSet)connector.callableStatement.getObject(1);
            return connector.readCursor();

        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Boolean addUser(User user) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()) {
            connector.openConnection();
            System.out.println("dao");
            connector.callableStatement = connector.connection.prepareCall("{call evoting.add_user(?,?,?,?,?,?,?,?)}");
            String passw = Utils.toSHA1(user.getPasswrd().getBytes());
            connector.callableStatement.setString(1, user.getUsername());
            connector.callableStatement.setString(2, passw);
            connector.callableStatement.setInt(3, user.getUserType());
            connector.callableStatement.setLong(4, user.getSurveyMember().getSurvey().getId());
            connector.callableStatement.setString(5, user.getFirstName());
            connector.callableStatement.setString(6, user.getLastName());
            connector.callableStatement.setString(7, user.getMiddleName());
            connector.callableStatement.setLong(8, user.getSurveyMember().getCountry().getId());
            connector.callableStatement.executeUpdate();
            connector.connection.commit();
            return true;
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }

    }

    @Override
    public Long editUser(User user) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.edit_user(?,?,?,?,?,?,?,?,?)}");
            connector.callableStatement.registerOutParameter(1, Types.NUMERIC);
            connector.callableStatement.setInt(2, user.getUserId());
            connector.callableStatement.setString(3, user.getUsername());
            connector.callableStatement.setString(4, user.getPasswrd().trim().length() > 0 ? Utils.toSHA1(user.getPasswrd().getBytes()): "");
            connector.callableStatement.setInt(5, user.getUserType());
            connector.callableStatement.setLong(6, user.getSurveyMember().getSurvey().getId());
            connector.callableStatement.setString(7, user.getFirstName());
            connector.callableStatement.setString(8, user.getLastName());
            connector.callableStatement.setString(9, user.getMiddleName());
            connector.callableStatement.setLong(10, user.getSurveyMember().getCountry().getId());

            connector.callableStatement.execute();

            connector.connection.commit();
            return ((BigDecimal) connector.callableStatement.getObject(1)).longValue();

        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Long deleteUser(long id) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance() ){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.delete_user(?)}");
            connector.callableStatement.registerOutParameter(1, Types.NUMERIC);
            connector.callableStatement.setLong(2, id);
            connector.callableStatement.execute();
            connector.connection.commit();
            return ((BigDecimal) connector.callableStatement.getObject(1)).longValue();
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Boolean resetPassword(User user) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()) {
            connector.openConnection();
            String passwordOriginal = getUserPassword();
            String password = toSHA1(passwordOriginal.getBytes());
            connector.callableStatement = connector.connection.prepareCall("{call evoting.reset_password(?,?,?)}");
            connector.callableStatement.setInt(1, user.getUserId());
            connector.callableStatement.setString(2, password);
            connector.callableStatement.setString(3, passwordOriginal);
            connector.callableStatement.execute();
            connector.connection.commit();
            return true;

        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }

    }

}
