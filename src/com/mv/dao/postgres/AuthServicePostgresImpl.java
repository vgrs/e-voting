package com.mv.dao.postgres;

import com.mv.domain.*;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.service.AuthService;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class AuthServicePostgresImpl implements AuthService {

    static final Logger logger = FactoryLogger.getLogger();

    @Override
    public User authenticate(Credentials credentials) throws AuthenticationException {

        UserPasswordCredentials userPasswordCredentials = (UserPasswordCredentials) credentials;
        User user = null;

        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance();){
            connector.openConnection();

            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.login(?,?)}");
            connector.callableStatement.registerOutParameter(1, Types.OTHER);
            connector.callableStatement.setString(2, userPasswordCredentials.getUserName());
            connector.callableStatement.setString(3, Utils.toSHA1(userPasswordCredentials.getPwdHash().getBytes()));
            connector.callableStatement.execute();
            connector.resultSet = (ResultSet) connector.callableStatement.getObject(1);
            if (connector.resultSet.next()){
                user = new User();
                user.setUserId(connector.resultSet.getInt("id"));
                user.setFirstName(connector.resultSet.getString("first_name"));
                user.setLastName(connector.resultSet.getString("last_name"));
                user.setUsername(connector.resultSet.getString("username"));
                user.setUserType(connector.resultSet.getInt("type_id"));

                if (user.getUserType() == User.UserTypes.Client.value ){
                    SurveyMember surveyMember = new SurveyMember();
                    surveyMember.setId(connector.resultSet.getInt("surver_member_id"));
                    surveyMember.setSurvey(new Survey(connector.resultSet.getInt("survey_id") , connector.resultSet.getString("survey_name")));
                    surveyMember.setCountry(new Country(connector.resultSet.getInt("country_id") ,   connector.resultSet.getString("country_name")));

                    user.setSurveyMember(surveyMember);
                }

            }else {
                throw new AuthenticationException();
            }
        } catch (SQLException se){
            logger.error(se);
            throw new AuthenticationException();
        } catch (UserDefinedException e) {
            logger.error(e);
            throw new AuthenticationException();
        }

        return user;
    }
}
