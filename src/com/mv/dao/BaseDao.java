package com.mv.dao;

import com.mv.domain.User;

public class BaseDao {

    private User loggedUser;

    public BaseDao(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    public User getLoggedUser() {
        return loggedUser;
    }
}
