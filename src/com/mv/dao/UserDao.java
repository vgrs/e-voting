package com.mv.dao;

import com.mv.domain.DataContent;
import com.mv.domain.SurveyMember;
import com.mv.domain.User;
import com.mv.domain.exceptions.UserDefinedException;
import com.sun.org.apache.xpath.internal.operations.Bool;

/**Created by a.maleyka on 10-Jun-17*/
public interface UserDao {

    DataContent getUsers(Integer surveyId, Integer userType, String search) throws UserDefinedException;
    DataContent getParticipantDetails(long userId) throws UserDefinedException;
    Boolean addUser(User user) throws UserDefinedException;
    Long editUser(User user) throws UserDefinedException;
    Long deleteUser(long id) throws UserDefinedException;
    Boolean resetPassword(User user) throws UserDefinedException;

}
