package com.mv.dao;

import com.mv.domain.DataContent;
import com.mv.domain.exceptions.UserDefinedException;


public interface ReportDao {

    DataContent getQuestResult(int surveyId) throws UserDefinedException;
    DataContent[] getResultByQuest(int questId) throws UserDefinedException;
}
