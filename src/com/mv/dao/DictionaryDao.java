package com.mv.dao;

import com.mv.domain.DataContent;
import com.mv.domain.Dictionary;
import com.mv.domain.exceptions.UserDefinedException;

public interface DictionaryDao {

    public DataContent getDictionaries( int dicTypeId ) throws UserDefinedException;

    public DataContent getCountries() throws UserDefinedException;

    public boolean addDictionary(Dictionary dictionary) throws UserDefinedException;

    public boolean editDictionary(Dictionary dictionary) throws UserDefinedException;

    public boolean deleteDictionary(long id) throws UserDefinedException;


}
