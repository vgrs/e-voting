package com.mv.utils.logger;

import com.mv.domain.exceptions.UserDefinedException;

public class LoggerUtils {

    public static LoggerLevels CURRENT_LOG_LEVEL = LoggerLevels.INFO;

    public static LoggerTypes CURRENT_LOGGER_TYPE = LoggerTypes.SYSTEM_OUT;

    public static String CURRENT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    public static LoggerLevels getLoggerLevels(String name) throws UserDefinedException {
        for ( LoggerLevels logLevel : LoggerLevels.values()){
            if(logLevel.name().equals(name)) return logLevel;
        }
        throw new UserDefinedException(0, "Cannot find log level");
    }


    public static LoggerTypes getLoggerTypes(String name) throws UserDefinedException{
        for ( LoggerTypes loggerTypes : LoggerTypes.values()){
            if(loggerTypes.name().equals(name)) return loggerTypes;
        }
        throw new UserDefinedException(0, "Cannot find log type");
    }

    public static void setCurrentLogLevel( String currentLogLevel) throws UserDefinedException {
        CURRENT_LOG_LEVEL = getLoggerLevels(currentLogLevel);
    }

    public static void setCurrentLoggerType(String currentLoggerType) throws UserDefinedException {
        CURRENT_LOGGER_TYPE = getLoggerTypes(currentLoggerType);
    }

    public static void setCurrentDateTimeFormat(String dateTimeFormat) {
        CURRENT_DATE_TIME_FORMAT = dateTimeFormat;
    }

}
