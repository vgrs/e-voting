package com.mv.utils;

public class Constants {

    public static final String ACTION = "action";
    public static final String TOKEN = "token";

    public static final String SUB_ACTION = "sub_action";
    public static final String LOGGED_USER = "logged_user";
    public static final String ERROR_MESSAGE = "error_message";
    public static final String PAGE_LOGIN = "login";
    public static final String PAGE_ADMIN = "admin";
    public static final String PAGE_CLIENT = "client";

    public static enum SERVLET_PATHS {

        LOGIN("/login"),
        CONTROLLER("/cs"),
        DICTIONARY("/ds"),
        COMMON("/com"),
        USERS("/users"),
        SURVEY("/survey"),
        QUESTIONNAIRE("/quest"),
        UTILS("/utils");

        public String value;

        SERVLET_PATHS(String value) {
            this.value = value;
        }
    }

    public static enum SUB_ACTIONS{
        getDataContent,
        getUsersList,
        login,
        addUser,
        editUser,
        deleteUser,
        addSurvey,
        editSurvey,
        deleteSurvey,
        addQuestionnaire,
        deleteQuestionnaire,
        startQuestionnaire,
        answerQuestionnaire,
        stopSurvey,
        resetPassword,
        getSurveyDetails,
        getCurrentSurveyDetails,
        getParticipantDetails,

        getWebSocketUrl
    }

    public static enum DB_PROVIDER {
        MYSQL,
        POSTGRESQL
    }

    public enum ResultStatus {
        SUCCESS,
        WARNING,
        ERROR
    }

    public static final String DATA_SOURCE_POSTGRESQL = "java:comp/env/jdbc/postgres";

    public static String HEADER_AUTHENTICATION_TOKEN = "AUTHENTICATION_TOKEN";
    public static String HEADER_RESULT_CODE = "RESULT_CODE";
    public static String HEADER_RESULT_MESSAGE = "RESULT_MESSAGE";
    public static String HEADER_RESULT_STATUS = "RESULT_STATUS";

}