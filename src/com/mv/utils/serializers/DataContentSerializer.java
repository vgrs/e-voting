package com.mv.utils.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.mv.domain.DataContent;

import java.io.IOException;

public class DataContentSerializer extends JsonSerializer<DataContent> {

    private static final String DATA = "data";

    @Override
    public void serialize(DataContent value, JsonGenerator jgen,
                          SerializerProvider provider) throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeArrayFieldStart(DATA);
        for (String [] row : value.getMetaData()){
            jgen.writeStartObject();
            for (String column : value.getColumns().keySet() ){
                jgen.writeStringField(column , row[value.getColumns().get(column)]);
            }
            jgen.writeEndObject();
        }
        jgen.writeEndArray();
        jgen.writeEndObject();
    }

}