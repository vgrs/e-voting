package com.mv.utils;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {

    private static final Logger logger = FactoryLogger.getLogger();

    public static String toSHA1(byte [] dataBytes) throws UserDefinedException {
        try{
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(dataBytes, 0, dataBytes.length);
            byte[] md5Bytes = md.digest();

            StringBuilder hexString = new StringBuilder();

            for (int i=0; i < md5Bytes.length; i++) {
                String hex=Integer.toHexString(0xff &   md5Bytes[i]);
                if(hex.length()==1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            logger.error(e);
            throw new UserDefinedException(1111, e.getMessage());
        }
    }

    public static <T> void serializeAndWrite2(Class<? extends T> cl, JsonSerializer<T> serializer, Object targetObject, OutputStream outputStream) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(cl, serializer);
        objectMapper.registerModule(module);
        objectMapper.writeValue(outputStream, targetObject);

        outputStream.close();
    }


    public static void serializeAndWrite2(Object targetObject, OutputStream outputStream) throws IOException {
        logger.trace();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(outputStream, targetObject);
        logger.trace();
        outputStream.close();
    }

    public static <T> String serialize(Object targetObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String ret = objectMapper.writeValueAsString(targetObject);

        FactoryLogger.getLogger().trace();

        return ret;
    }

}
