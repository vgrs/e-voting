package com.mv.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv.domain.User;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.security.Key;
import java.util.Calendar;
import java.util.Date;

public class JWT {

    private static Logger logger = FactoryLogger.getLogger();

    private static final Key key = MacProvider.generateKey();
    private static final int TOKEN_EXPIRATION_TIME = 1200000;
    private static final int TOKEN_EXPIRATION_REFRESH_TIME = 600000;


    public static String buildToken(String jsonClaim){

        return buildToken(jsonClaim, true);
    }

    public static String buildToken(String jsonClaim, boolean expired){
        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        JwtBuilder jwtBuilder = Jwts.builder().setSubject(jsonClaim).setIssuedAt(new Date(t));
        if (expired){
            jwtBuilder.setExpiration(new Date(t + TOKEN_EXPIRATION_TIME));
        }
        return jwtBuilder.signWith(SignatureAlgorithm.HS512, key).compact();
    }


    public static String buildToken(User user, boolean expired) throws UserDefinedException {
        try{
            ObjectMapper mapper = new ObjectMapper();
            String userAsJson = mapper.writeValueAsString(user);

            return JWT.buildToken(userAsJson, expired);
        }catch (JsonProcessingException e) {
            logger.error(e);
            throw new UserDefinedException(0, e.getMessage());
        }

    }

    public static String buildToken(User user) throws UserDefinedException {
        try{
            ObjectMapper mapper = new ObjectMapper();
            String userAsJson = mapper.writeValueAsString(user);

            return JWT.buildToken(userAsJson);
        }catch (JsonProcessingException e) {
            logger.error(e);
            throw new UserDefinedException(0, e.getMessage());
        }

    }


    public static Jws<Claims> checkToken( String token) throws AuthenticationException {
        if (token == null) throw new AuthenticationException();
        try{
            return Jwts.parser().setSigningKey(key).parseClaimsJws(token);
        }catch (Exception e){

            throw new AuthenticationException();
        }
    }


    public static User checkToken4User(String token) throws AuthenticationException{
        Logger logger = FactoryLogger.getLogger();

        try{
            if (token == null) throw new AuthenticationException();

            logger.trace("token is not null");

            Jws<Claims> claimsJws = checkToken(token);
            ObjectMapper mappper = new ObjectMapper();
            return mappper.readValue(claimsJws.getBody().getSubject(), User.class );
        }catch (Exception e){
            e.printStackTrace();
            logger.trace("Exception in checkToken4User" + e);
            throw new AuthenticationException();
        }
    }

    public static String refreshToken(String token) throws AuthenticationException{
        Calendar date = Calendar.getInstance();
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(key).parseClaimsJws(token);

        if (claimsJws != null ){
            if ( claimsJws.getBody().getExpiration().getTime() -  date.getTimeInMillis() <= TOKEN_EXPIRATION_REFRESH_TIME){
                return buildToken(claimsJws.getBody().getSubject());
            }
            return token;
        }
        throw new AuthenticationException();
    }

}
