ALTER TABLE evoting.users DROP CONSTRAINT constraint_name; //username unique contrainti yigisdirir

/*---------------------------------function add survey------------------------------------------------------*/
CREATE or REPLACE FUNCTION evoting.add_survey (p_name text, p_user_id numeric, member_list text[]) RETURNS numeric
	LANGUAGE plpgsql
AS $$
DECLARE
  v_id NUMERIC;
  v_count NUMERIC;
  m TEXT[];
  v_participant_id  NUMERIC;
  v_person_id NUMERIC;
  m1 NUMERIC;
  v_user_id NUMERIC;

BEGIN

  v_id := nextval('evoting."seq_survey"');

	update evoting.survey
	set
		end_date = CURRENT_TIMESTAMP
	where end_date is null;

  INSERT INTO evoting.survey (id, name, create_date, update_user_id, active)
    VALUES (v_id, p_name, current_date, p_user_id, 1);

   FOREACH m SLICE 1 IN ARRAY member_list

   LOOP

     m1 := to_number(m[1], '999999999999');

     v_participant_id := nextval('evoting."seq_quest_variant"');

      v_person_id := nextval('evoting."seq_person"');
      INSERT INTO evoting.person (id,
                                  first_name,
                                  country_id,
                                  active)
                          VALUES (v_person_id,
                                  m[2],
                                  m1,
                                  1);
      v_user_id := nextval('evoting."seq_users"') ;
      INSERT INTO evoting.users (id,
                                 person_id,
                                 username,
                                 passwrd,
                                 type_id,
                                 active,
                                 create_date)
                        VALUES (v_user_id,
                                v_person_id,
                                m[2],
                                '123',
                                2,
                                1,
                                current_date);


      INSERT INTO evoting.survey_members(id,
                                         survey_id,
                                         user_id,
                                         country_id,
                                         active)
                                VALUES (nextval('evoting.seq_survey_members'),
                                        v_id,
                                        v_user_id,
                                        m1,
                                        1 );


   END LOOP;


  RETURN v_id;

END
$$
/*-----------------------------------end------------------------------------------------------------*/


------------------------------------------"evoting"."add_questionnaire"-------------------------------------------

CREATE OR REPLACE FUNCTION "evoting"."add_questionnaire"(p_type_id numeric, p_privacy_type numeric, p_content text, p_time_interval numeric, p_vote_count numeric, p_variants _varchar)
  RETURNS "pg_catalog"."void" AS $BODY$DECLARE
  v_id NUMERIC;
  m VARCHAR[];
  v_variant_id NUMERIC;
  v_variant_content TEXT;
  i NUMERIC;
BEGIN


      v_id := nextval('evoting."seq_questionnaire"');

  INSERT INTO evoting.questionnaire (id, type_id, privacy_type, content, time_interval, active, create_date, vote_count)
    VALUES (v_id, p_type_id, p_privacy_type, p_content, p_time_interval, 1, current_date, p_vote_count);


  /*----------------------variantlarin sual tipine gore elave olunmasi-----------------------*/
  IF p_type_id = 100 THEN

    v_variant_id := nextval('evoting."seq_quest_variant"');
    INSERT INTO evoting.quest_variant(id, quest_id, active, country_id, content)
    SELECT v_variant_id, v_id, 1, id, name FROM evoting.dictionary d WHERE d.type_id = 1004;

  ELSE
       FOREACH m SLICE 1 IN ARRAY p_variants
       LOOP
         i := 1;

         LOOP
           EXIT WHEN m[i] is NULL  ;

           --SELECT c.name into v_variant_content from evoting.countries c WHERE c.id = to_number(m[i], '9999999999999');

           v_variant_id := nextval('evoting."seq_quest_variant"');

           INSERT INTO evoting.quest_variant (id, quest_id, country_id, content)
           VALUES (v_variant_id, v_id, 0, m[i]);

           i := i + 1 ;
         END LOOP ;

       END LOOP;

  END IF;
/*---------------------------------end variants---------------------------------------*/

END
$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;


---------------------------------------------evoting.get_users---------------------------------------------


CREATE OR REPLACE FUNCTION "evoting"."get_users"(p_survey_id numeric, p_user_type_id numeric, p_search text)
  RETURNS "pg_catalog"."refcursor" AS $BODY$

DECLARE
  v_id NUMERIC;
  v_out REFCURSOR;
BEGIN

  OPEN v_out FOR
    SELECT
      u.id,
      u.username,
      u.person_id,
      u.type_id user_type_id,
      d.name user_type,
      p.first_name || ' ' ||p.last_name || ' ' ||p.middle_name as full_name,
      p.first_name,
      p.last_name,
      p.middle_name,
      c.name country_name,
      c.id country_id,
      s.name survey_name,
      s.id survey_id
    FROM evoting.users u
    JOIN evoting.dictionary d on d.id = u.type_id and d.active = 1
    JOIN evoting.person p on p.id = u.person_id and p.active = 1
    LEFT JOIN evoting.survey_members m on m.user_id = u.id AND m.active = 1
    left JOIN evoting.countries c on c.id = m.country_id and c.active = 1
    LEFT JOIN evoting.survey s on s.id = u.survey_id AND s.active = 1
    WHERE --u.username LIKE '%'||||'%';
    (u.type_id = p_user_type_id or p_user_type_id = 0) AND u.active = 1
    AND (u.survey_id = p_survey_id or p_survey_id = 0 or ( p_survey_id = 1 and u.survey_id is null) )
  ;

  RETURN v_out;

END

$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;


------------------------------------------------------

---------------password for the client----------------
ALTER TABLE evoting.users ADD password_original TEXT NULL;


--------------------------------------------------------add_survey 13-07-2017-----------------------------------------
CREATE or REPLACE FUNCTION evoting.add_survey (p_name text, p_user_id numeric, member_list text[]) RETURNS numeric
	LANGUAGE plpgsql
AS $$
DECLARE
  v_id NUMERIC;
  v_count NUMERIC;
  m TEXT[];
  v_participant_id  NUMERIC;
  v_person_id NUMERIC;
  m1 NUMERIC;
  v_user_id NUMERIC;
  v_password TEXT;

BEGIN

  v_id := nextval('evoting."seq_survey"');

	update evoting.survey
	set
		end_date = CURRENT_TIMESTAMP
	where end_date is null;

  INSERT INTO evoting.survey (id, name, create_date, update_user_id, active)
    VALUES (v_id, p_name, current_date, p_user_id, 1);

   FOREACH m SLICE 1 IN ARRAY member_list

   LOOP
     m1 := to_number(m[1], '999999999999');

     v_participant_id := nextval('evoting."seq_quest_variant"');

      v_person_id := nextval('evoting."seq_person"');
      INSERT INTO evoting.person (id,
                                  first_name,
                                  country_id,
                                  active)
                          VALUES (v_person_id,
                                  m[2],
                                  m1,
                                  1);
      v_user_id := nextval('evoting."seq_users"');

     SELECT substring(md5(random()::TEXT) FOR 10) INTO v_password;

      INSERT INTO evoting.users (id,
                                 person_id,
                                 username,
                                 passwrd,
                                 type_id,
                                 active,
                                 create_date,
                                 password_original)
                        VALUES (v_user_id,
                                v_person_id,
                                nvl(m[2],
                                md5(v_password),
                                2,
                                1,
                                current_date,
                                v_password);


      INSERT INTO evoting.survey_members(id,
                                         survey_id,
                                         user_id,
                                         country_id,
                                         active)
                                VALUES (nextval('evoting.seq_survey_members'),
                                        v_id,
                                        v_user_id,
                                        m1,
                                        1 );


   END LOOP;


  RETURN v_id;

END
$$

---------------------------------------MALEYKA 14_07_2017----------------------------------------

CREATE OR REPLACE FUNCTION evoting.get_users(
	p_survey_id numeric,
	p_user_type_id numeric,
	p_search text)
RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE
AS $function$

DECLARE
  v_id NUMERIC;
  v_out REFCURSOR;
BEGIN

  OPEN v_out FOR
    SELECT
      u.id,
      u.username,
      u.person_id,
      u.type_id user_type_id,
      d.name user_type,
      p.first_name || ' ' ||p.last_name || ' ' ||p.middle_name as full_name,
      p.first_name,
      p.last_name,
      p.middle_name,
      c.name country_name,
      c.id country_id,
      s.name survey_name,
      s.id survey_id
    FROM evoting.users u
    JOIN evoting.dictionary d on d.id = u.type_id and d.active = 1
    JOIN evoting.person p on p.id = u.person_id and p.active = 1
    LEFT JOIN evoting.survey_members m on m.user_id = u.id AND m.active = 1
    left JOIN evoting.countries c on c.id = m.country_id and c.active = 1
    LEFT JOIN evoting.survey s on s.id = u.survey_id AND s.active = 1
    WHERE --u.username LIKE '%'||||'%';
    (u.type_id = p_user_type_id or p_user_type_id = 0) AND u.active = 1
    AND (m.survey_id = p_survey_id or p_survey_id = 0)
  ;

  RETURN v_out;

END

$function$;

ALTER FUNCTION evoting.get_users(numeric, numeric, text)
    OWNER TO vuqar_evoting;

---------------------------------------------------------------------------------------------

----------------------------------------------14_07_2017------------------------
CREATE or REPLACE FUNCTION evoting.add_survey (p_name text, p_user_id numeric, member_list text[]) RETURNS numeric
	LANGUAGE plpgsql
AS $$
DECLARE
  v_id NUMERIC;
  v_count NUMERIC;
  m TEXT[];
  v_participant_id  NUMERIC;
  v_person_id NUMERIC;
  m1 NUMERIC;
  v_user_id NUMERIC;
  v_password TEXT;

BEGIN

  v_id := nextval('evoting."seq_survey"');

	update evoting.survey
	set
		end_date = CURRENT_TIMESTAMP
	where end_date is null;

  INSERT INTO evoting.survey (id, name, create_date, update_user_id, active)
    VALUES (v_id, p_name, current_date, p_user_id, 1);

   FOREACH m SLICE 1 IN ARRAY member_list

   LOOP
      m1 := to_number(m[1], '999999999999');

      v_participant_id := nextval('evoting."seq_quest_variant"');

      v_person_id := nextval('evoting."seq_person"');
      INSERT INTO evoting.person (id,
                                  first_name,
                                  country_id,
                                  active)
                          VALUES (v_person_id,
                                  m[2],
                                  m1,
                                  1);
      v_user_id := nextval('evoting."seq_users"');

      INSERT INTO evoting.users (id,
                                 person_id,
                                 username,
                                 passwrd,
                                 type_id,
                                 active,
                                 create_date,
                                 password_original)
                        VALUES (v_user_id,
                                v_person_id,
                                m[2],
                                m[3],
                                2,
                                1,
                                current_date,
                                m[4]);


      INSERT INTO evoting.survey_members(id,
                                         survey_id,
                                         user_id,
                                         country_id,
                                         active)
                                VALUES (nextval('evoting.seq_survey_members'),
                                        v_id,
                                        v_user_id,
                                        m1,
                                        1 );


   END LOOP;


  RETURN v_id;



END
$$
-------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------14_07_2017-------------------------------------------------------------
CREATE FUNCTION evoting.reset_password (p_user_id numeric, p_password text, p_password_original text) RETURNS void
	LANGUAGE plpgsql
AS $$
DECLARE
  begin

    UPDATE evoting.users
      SET password_original = p_password_original, passwrd = p_password
    WHERE id = p_user_id;

  end

$$


--------------------------------------------------------------------------------------------------------------------------------------

--------------------------------edit user password yenilenmeyecek------------------------------------------------------------------
CREATE or REPLACE FUNCTION evoting.edit_user (p_id numeric, p_username text, p_passwrd text, p_type_id numeric, p_survey_id numeric, p_first_name text, p_last_name text, p_middle_name text, p_country_id numeric) RETURNS numeric
	LANGUAGE plpgsql
AS $$

DECLARE

  v_person_id NUMERIC;
  v_count numeric;

BEGIN
/*

  SELECT u.person_id
  INTO v_person_id
  FROM evoting.users u
  WHERE u.id = p_id;

  UPDATE evoting.person
  SET first_name = p_first_name, last_name = p_last_name, middle_name = p_middle_name,
    country_id   = p_country_id

  WHERE id = v_person_id;

  UPDATE evoting.users
  SET username = p_username, passwrd = p_passwrd, type_id = p_type_id, survey_id = p_survey_id
  WHERE id = p_id;



 if p_survey_id > 0 then
 -- muvafiq survey-e olke melumatini redakte edir----------------

  	select count(*) into v_count from evoting.survey_members m where m.survey_id = p_survey_id and user_id = p_id and active = 1;
    if v_count > 0 then
      UPDATE evoting.survey_members
      SET country_id = p_country_id
      WHERE survey_id = p_survey_id and user_id = p_id and active = 1;
    else
    	insert into evoting.survey_members  (id, country_id, survey_id, user_id, active)
        values(nextval('evoting.seq_survey_members'), p_country_id, p_survey_id, p_id, 1);
    end if;
 end if;

  RETURN p_id;
*/


  SELECT u.person_id
  INTO v_person_id
  FROM evoting.users u
  WHERE u.id = p_id;

  UPDATE evoting.person
  SET first_name = p_first_name, last_name = p_last_name, middle_name = p_middle_name,
    country_id   = p_country_id

  WHERE id = v_person_id;

  UPDATE evoting.users
  SET username = p_username, type_id = p_type_id, survey_id = p_survey_id
  WHERE id = p_id;



 if p_survey_id > 0 then
 -- muvafiq survey-e olke melumatini redakte edir----------------

  	select count(*) into v_count from evoting.survey_members m where m.survey_id = p_survey_id and user_id = p_id and active = 1;
    if v_count > 0 then
      UPDATE evoting.survey_members
      SET country_id = p_country_id
      WHERE survey_id = p_survey_id and user_id = p_id and active = 1;
    else
    	insert into evoting.survey_members  (id, country_id, survey_id, user_id, active)
        values(nextval('evoting.seq_survey_members'), p_country_id, p_survey_id, p_id, 1);
    end if;
 end if;

  RETURN p_id;

END

$$
-----------------------------------------edit user end------------------------------------------------------------


/*----------------------23_07_2017--------------------*/
-- FUNCTION: evoting.get_report_by_survey(numeric)

-- DROP FUNCTION evoting.get_report_by_survey(numeric);

CREATE OR REPLACE FUNCTION evoting.get_report_by_survey(
	p_survey_id numeric)
RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE
AS $function$
DECLARE

v_out REFCURSOR;

v_survey_id NUMERIC;

BEGIN

  if p_survey_id =0 THEN

    SELECT  id INTO v_survey_id from evoting.survey s where s.end_date is null;

  ELSE v_survey_id := p_survey_id ;

  END IF;

   open v_out FOR

    SELECT v.content result_variant,  coalesce( q.winner_count, 1) winner_count,

      uv.quest_move_id,

      uv.quest_variant_id,

      q."content" ,m.id,

      m.start_date,

                       sum(uv.vote_count) votes_sum

                      FROM  evoting.survey s

                      JOIN evoting.quest_movement m ON m.survey_id = s.id AND m.active = 1

                      JOIN evoting.questionnaire q ON q.id = m.quest_id AND q.active = 1

                      JOIN evoting.quest_variant v ON v.quest_id = q.id AND v.active = 1

                      JOIN evoting.survey_members sm ON sm.survey_id = s.id AND sm.active = 1

                      JOIN evoting.user_votes uv ON uv.survey_member_id = sm.id AND uv.active = 1

                                                 AND uv.quest_variant_id = v.id and uv.quest_move_id = m.id

      WHERE v.active = 1 AND s.id = v_survey_id

                     GROUP BY  uv.quest_move_id,uv.quest_variant_id,q."content",v.content,q.winner_count,

                m.id,

                m.start_date

    ORDER BY uv.quest_move_id, votes_sum desc;

/*

SELECT  * from (

  SELECT

    CASE WHEN b.mk = sum(uv.vote_count)

      THEN v.content END result_variant,

    uv.quest_move_id,

    uv.quest_variant_id,

    q."content" ,m.id,

    m.start_date

  FROM evoting.survey s

    JOIN evoting.quest_movement m ON m.survey_id = s.id AND m.active = 1

    JOIN evoting.questionnaire q ON q.id = m.quest_id AND q.active = 1

    JOIN evoting.quest_variant v ON v.quest_id = q.id AND v.active = 1

    JOIN evoting.survey_members sm ON sm.survey_id = s.id AND sm.active = 1

    JOIN evoting.user_votes uv ON uv.survey_member_id = sm.id AND uv.active = 1

                                  AND uv.quest_variant_id = v.id and uv.quest_move_id = m.id

    JOIN (SELECT

            q."content",

            m.id,

            m.start_date,

            max(a.votes_sum) mk

          FROM evoting.survey s

            JOIN evoting.quest_movement m ON m.survey_id = s.id AND m.active = 1

            JOIN evoting.questionnaire q ON q.id = m.quest_id AND q.active = 1

            JOIN evoting.quest_variant v ON v.quest_id = q.id AND v.active = 1

            JOIN evoting.survey_members sm ON sm.survey_id = s.id AND sm.active = 1

            JOIN evoting.user_votes uv ON uv.survey_member_id = sm.id AND sm.active = 1

            JOIN (SELECT uv.quest_variant_id,uv.quest_move_id,

                    sum(uv.vote_count) votes_sum

                  FROM  evoting.survey s

                  JOIN evoting.quest_movement m ON m.survey_id = s.id AND m.active = 1

                  JOIN evoting.questionnaire q ON q.id = m.quest_id AND q.active = 1

                  JOIN evoting.quest_variant v ON v.quest_id = q.id AND v.active = 1

                  JOIN evoting.survey_members sm ON sm.survey_id = s.id AND sm.active = 1

                  JOIN evoting.user_votes uv ON uv.survey_member_id = sm.id AND uv.active = 1

                    AND uv.quest_variant_id = v.id and uv.quest_move_id = m.id

  WHERE v.active = 1 AND s.id = v_survey_id

                 GROUP BY  uv.quest_move_id,uv.quest_variant_id) a ON a.quest_variant_id = v.id

          WHERE s.active = 1 AND s.id = v_survey_id

          GROUP BY q."content", m.id, m.start_date

          ORDER BY q."content") b ON b.id = m.id

  WHERE v.active = 1 AND s.id = v_survey_id

  GROUP BY v.id, v.content, uv.quest_move_id, uv.quest_variant_id, b.mk, b."content", m.id,q."content" ,m.id,

    m.start_date

)b

where b.result_variant is not null;

*/

  RETURN v_out;

END;

$function$;

ALTER FUNCTION evoting.get_report_by_survey(numeric)
    OWNER TO vuqar_evoting;


--------------------------------------------------------------
-- FUNCTION: evoting.start_questionnaire(numeric)

-- DROP FUNCTION evoting.start_questionnaire(numeric);

CREATE OR REPLACE FUNCTION evoting.start_questionnaire(
	p_quest_id numeric)
RETURNS numeric
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE
AS $function$


DECLARE

    v_id NUMERIC;

    v_count NUMERIC;

    v_time NUMERIC;

    v_survey_id     NUMERIC := 0;

BEGIN

			SELECT count(S.id) into v_count

          FROM evoting.survey s WHERE s.end_date is NULL;

			IF v_count = 0 THEN

				RAISE EXCEPTION 'There is no active survey. Please contact to administrator!';

			ELSIF v_count > 1 THEN

				RAISE EXCEPTION 'There is an duplicated active survey. Please contact to administrator!';

			END IF;

        SELECT S.id into v_survey_id

          FROM evoting.survey s WHERE s.end_date is NULL;

		v_count := 0;

    /*----------checks if questionnaire added to survey already or not-----------*/

    SELECT count(*)

    into v_count

    from evoting.quest_movement m

    WHERE m.survey_id = v_survey_id AND m.quest_id = p_quest_id and m.active = 1;

    /*-----------if added quest------------*/

    IF v_count = 1 THEN

        RAISE EXCEPTION 'Duplicated question!';

      /*--------------if not------------*/

    ELSE

        v_id := nextval('evoting."seq_quest_movement"');

        SELECT time_interval INTO v_time FROM evoting.questionnaire WHERE active = 1 AND id = p_quest_id;

        INSERT INTO evoting.quest_movement (id, quest_id, start_date, time_interval, survey_id, active)

        VALUES (v_id, p_quest_id, current_timestamp, v_time, v_survey_id, 1);

        INSERT INTO evoting.user_votes (id,

																				quest_move_id,

																				quest_variant_id,

																				user_id,

																				survey_member_id,

																				vote_count,

																				active)

-- 				SELECT nextval('evoting."seq_user_votes"'), v_id, v.id, sm.user_id, sm.id, 0, 1

SELECT  nextval('evoting."seq_user_votes"'),v_id ,qv.id , sm.user_id, sm.id,0,1

        FROM evoting.survey s

          join evoting.quest_movement m on m.survey_id = s.id and m.active = 1 and m.id = v_id

          join evoting.questionnaire q on q.id = m.quest_id and q.active = 1

          join evoting.survey_members sm on sm.survey_id = s.id

          join evoting.quest_variant qv on qv.quest_id = q.id

           where s.id = v_survey_id ;

    END IF;

    RETURN  v_id;

END

$function$;

ALTER FUNCTION evoting.start_questionnaire(numeric)
    OWNER TO vuqar_evoting;

------------------------------------------------------------------------------
-- FUNCTION: evoting.add_questionnaire(numeric, numeric, text, numeric, numeric, character varying[])

-- DROP FUNCTION evoting.add_questionnaire(numeric, numeric, text, numeric, numeric, character varying[]);

CREATE OR REPLACE FUNCTION evoting.add_questionnaire(
	p_type_id numeric,
	p_privacy_type numeric,
	p_content text,
	p_time_interval numeric,
	p_vote_count numeric,
    p_winner_count numeric,
	p_variants character varying[])
RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE
AS $function$


DECLARE

  v_id NUMERIC;

  m VARCHAR[];

  v_variant_id NUMERIC;

  v_variant_content TEXT;

  i NUMERIC;

BEGIN

      v_id := nextval('evoting."seq_questionnaire"');

  INSERT INTO evoting.questionnaire (id, type_id, privacy_type, content, time_interval, active, create_date, vote_count, winner_count)

    VALUES (v_id, p_type_id, p_privacy_type, p_content, p_time_interval, 1, current_date, p_vote_count, p_winner_count);

  /*----------------------variantlarin sual tipine gore elave olunmasi-----------------------*/

  IF p_type_id = 100 THEN

    v_variant_id := nextval('evoting."seq_quest_variant"');

    INSERT INTO evoting.quest_variant(id, quest_id, active, country_id, content)

    SELECT nextval('evoting."seq_quest_variant"'), v_id, 1, id, name FROM evoting.dictionary d WHERE d.type_id = 1004;

  ELSE

       FOREACH m SLICE 1 IN ARRAY p_variants

       LOOP

         i := 1;

         LOOP

           EXIT WHEN m[i] is NULL  ;

           INSERT INTO evoting.quest_variant (id, quest_id, country_id, content)

           VALUES (nextval('evoting."seq_quest_variant"'), v_id, 0, m[i]);

           i := i + 1 ;

         END LOOP ;

       END LOOP;

  END IF;

/*---------------------------------end variants---------------------------------------*/

END

$function$;

ALTER FUNCTION evoting.add_questionnaire(numeric, numeric, text, numeric, numeric, character varying[])
    OWNER TO vuqar_evoting;

------------------------------------------------------------------------------------
-- FUNCTION: evoting.get_report_by_survey(numeric)

-- DROP FUNCTION evoting.get_report_by_survey(numeric);

CREATE OR REPLACE FUNCTION evoting.get_report_by_survey(
	p_survey_id numeric)
RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE
AS $function$

DECLARE

v_out REFCURSOR;

v_survey_id NUMERIC;

BEGIN

  if p_survey_id =0 THEN

    SELECT  id INTO v_survey_id from evoting.survey s where s.end_date is null;

  ELSE v_survey_id := p_survey_id ;

  END IF;

   open v_out FOR

    SELECT v.content result_variant,  coalesce( q.winner_count, 1) winner_count,
      uv.quest_move_id,
      uv.quest_variant_id,
      q."content" ,m.id,
      m.start_date,
      sum(uv.vote_count) votes_sum
                      FROM  evoting.survey s
                      JOIN evoting.quest_movement m ON m.survey_id = s.id AND m.active = 1
                      JOIN evoting.questionnaire q ON q.id = m.quest_id AND q.active = 1
                      JOIN evoting.quest_variant v ON v.quest_id = q.id AND v.active = 1
                      JOIN evoting.survey_members sm ON sm.survey_id = s.id AND sm.active = 1
                      JOIN evoting.user_votes uv ON uv.survey_member_id = sm.id AND uv.active = 1
                                                 AND uv.quest_variant_id = v.id and uv.quest_move_id = m.id
      WHERE v.active = 1 AND s.id = v_survey_id
                     GROUP BY  uv.quest_move_id,uv.quest_variant_id,q."content",v.content,q.winner_count,
                m.id,
                m.start_date
    ORDER BY uv.quest_move_id, votes_sum desc, v.content asc;

/*

SELECT  * from (

  SELECT

    CASE WHEN b.mk = sum(uv.vote_count)

      THEN v.content END result_variant,

    uv.quest_move_id,

    uv.quest_variant_id,

    q."content" ,m.id,

    m.start_date

  FROM evoting.survey s

    JOIN evoting.quest_movement m ON m.survey_id = s.id AND m.active = 1

    JOIN evoting.questionnaire q ON q.id = m.quest_id AND q.active = 1

    JOIN evoting.quest_variant v ON v.quest_id = q.id AND v.active = 1

    JOIN evoting.survey_members sm ON sm.survey_id = s.id AND sm.active = 1

    JOIN evoting.user_votes uv ON uv.survey_member_id = sm.id AND uv.active = 1

                                  AND uv.quest_variant_id = v.id and uv.quest_move_id = m.id

    JOIN (SELECT

            q."content",

            m.id,

            m.start_date,

            max(a.votes_sum) mk

          FROM evoting.survey s

            JOIN evoting.quest_movement m ON m.survey_id = s.id AND m.active = 1

            JOIN evoting.questionnaire q ON q.id = m.quest_id AND q.active = 1

            JOIN evoting.quest_variant v ON v.quest_id = q.id AND v.active = 1

            JOIN evoting.survey_members sm ON sm.survey_id = s.id AND sm.active = 1

            JOIN evoting.user_votes uv ON uv.survey_member_id = sm.id AND sm.active = 1

            JOIN (SELECT uv.quest_variant_id,uv.quest_move_id,

                    sum(uv.vote_count) votes_sum

                  FROM  evoting.survey s

                  JOIN evoting.quest_movement m ON m.survey_id = s.id AND m.active = 1

                  JOIN evoting.questionnaire q ON q.id = m.quest_id AND q.active = 1

                  JOIN evoting.quest_variant v ON v.quest_id = q.id AND v.active = 1

                  JOIN evoting.survey_members sm ON sm.survey_id = s.id AND sm.active = 1

                  JOIN evoting.user_votes uv ON uv.survey_member_id = sm.id AND uv.active = 1

                    AND uv.quest_variant_id = v.id and uv.quest_move_id = m.id

  WHERE v.active = 1 AND s.id = v_survey_id

                 GROUP BY  uv.quest_move_id,uv.quest_variant_id) a ON a.quest_variant_id = v.id

          WHERE s.active = 1 AND s.id = v_survey_id

          GROUP BY q."content", m.id, m.start_date

          ORDER BY q."content") b ON b.id = m.id

  WHERE v.active = 1 AND s.id = v_survey_id

  GROUP BY v.id, v.content, uv.quest_move_id, uv.quest_variant_id, b.mk, b."content", m.id,q."content" ,m.id,

    m.start_date

)b

where b.result_variant is not null;

*/

  RETURN v_out;

END;

$function$;

ALTER FUNCTION evoting.get_report_by_survey(numeric)
    OWNER TO vuqar_evoting;


/*----------------------------end 23_07_2017------------------------------------*/