package com.mv.domain;

public class DataContentArray{

    private DataContent[] dataContents;

    public DataContent[] getDataContents() {
        return dataContents;
    }

    public void setDataContents(DataContent[] dataContents) {
        this.dataContents = dataContents;
    }
}
