package com.mv.domain;

import java.io.Serializable;
import java.util.List;

public class QuestionnaireAnswerWrapper implements Serializable {


    private QuestionnaireAnswer [] questionnaireAnswerList;


    public QuestionnaireAnswerWrapper(QuestionnaireAnswer[] questionnaireAnswerList) {
        this.questionnaireAnswerList = questionnaireAnswerList;
    }

    public QuestionnaireAnswerWrapper() {

    }

    public QuestionnaireAnswer[] getQuestionnaireAnswerList() {
        return questionnaireAnswerList;
    }

    public void setQuestionnaireAnswerList(QuestionnaireAnswer[] questionnaireAnswerList) {
        this.questionnaireAnswerList = questionnaireAnswerList;
    }
}
