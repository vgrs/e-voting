package com.mv.domain;

import com.mv.utils.Constants;

public class ResultMessage{

    private Constants.ResultStatus resultStatus;
    private Message message;

    public ResultMessage(Constants.ResultStatus resultStatus, Message message) {
        this.resultStatus = resultStatus;
        this.message = message;
    }

    public ResultMessage() {

    }

    public Constants.ResultStatus getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(Constants.ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
