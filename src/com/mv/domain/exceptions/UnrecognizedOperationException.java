package com.mv.domain.exceptions;


public class UnrecognizedOperationException extends UserDefinedException{

    public UnrecognizedOperationException(){
        super(20502, "UnrecognizedOperation");
    }
}
