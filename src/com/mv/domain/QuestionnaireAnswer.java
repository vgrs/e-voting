package com.mv.domain;

import java.io.Serializable;

public class QuestionnaireAnswer implements Serializable{

    //private long questMoveId ;
    private long variantId;
    private long voteCount;

//    public long getQuestMoveId() {
//        return questMoveId;
//    }
//
//    public void setQuestMoveId(long questMoveId) {
//        this.questMoveId = questMoveId;
//    }

    public long getVariantId() {
        return variantId;
    }

    public void setVariantId(long variantId) {
        this.variantId = variantId;
    }

    public long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(long voteCount) {
        this.voteCount = voteCount;
    }

}
