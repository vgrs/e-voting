package com.mv.domain;

import java.io.Serializable;
import java.util.List;

public class Survey implements Serializable{
    private long id;
    private String name;

    private List<SurveyMember> surveyMembers;

    public Survey(){}

    public Survey(long id, String name){
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SurveyMember> getSurveyMembers() {
        return surveyMembers;
    }

    public void setSurveyMembers(List<SurveyMember> surveyMembers) {
        this.surveyMembers = surveyMembers;
    }
}
