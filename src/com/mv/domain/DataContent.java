package com.mv.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mv.utils.serializers.DataContentSerializer;

import java.util.ArrayList;
import java.util.Map;

@JsonSerialize(using = DataContentSerializer.class)
public class DataContent extends AbstractObject {

    private Map<String, Integer> columns;

    private ArrayList<String[]> metaData;

    public Map<String, Integer> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, Integer> columns) {
        this.columns = columns;
    }

    public ArrayList<String[]> getMetaData() {
        return metaData;
    }

    public void setMetaData(ArrayList<String[]> metaData) {
        this.metaData = metaData;
    }


    /*@Override
    public String getJsonString() {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode root = mapper.createObjectNode();
        ArrayNode arrayNode = root.putArray(DATA);
        for (String [] row : metaData) {
            ObjectNode jsonRow = mapper.createObjectNode();
            for (String column : columns.keySet() ) {
                jsonRow.put(column,  row[columns.get(column)] );
            }
            arrayNode.add(jsonRow);
        }
        return super.getJsonString();
    }*/
}








