package com.mv.domain;

import com.mv.utils.Constants;

import java.io.Serializable;

public class User extends Person implements Serializable {

    public enum UserTypes{
        Admin(1),
        Client(2);

        public int value;
        UserTypes(int i) {
            this.value = i;
        }
    }

    private Integer userId;
    private Integer userType;
    private String username;
    private int langId;
    private String token;
    private String passwrd;

    private SurveyMember surveyMember;


    public User() {

    }

    public User(String name, String lastName , Integer userId, String userName, int userType) {
        setFirstName(name);
        setLastName(lastName);
        setUserId(userId);
        setUsername(userName);
        setUserType(userType);
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public int getLangId() {
        return langId;
    }

    public void setLangId(int langId) {
        this.langId = langId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPasswrd() {
        return passwrd;
    }

    public void setPasswrd(String passwrd) {
        this.passwrd = passwrd;
    }


    public SurveyMember getSurveyMember() {
        return surveyMember;
    }

    public void setSurveyMember(SurveyMember surveyMember) {
        this.surveyMember = surveyMember;
    }

    public boolean getPathAccess(String page){
        if (userType == UserTypes.Admin.value){
            return true;
        } else {
            return (page).equals( "/" + Constants.PAGE_CLIENT) || page.equals("/" + Constants.PAGE_LOGIN) || page.equals(Constants.SERVLET_PATHS.CONTROLLER.value);
        }
    }


}
