package com.mv.service;


import com.mv.domain.Credentials;
import com.mv.domain.User;
import com.mv.domain.exceptions.AuthenticationException;

public interface AuthService {

    User authenticate(Credentials credentials) throws AuthenticationException;

}
