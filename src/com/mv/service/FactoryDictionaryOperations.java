package com.mv.service;

import com.mv.dao.DictionaryDao;
import com.mv.dao.postgres.DictionaryDaoPostgresImpl;
import com.mv.domain.User;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Constants;


public class FactoryDictionaryOperations {

    public static DictionaryDao getDictionaryOperationProvider(User loggedUser , Constants.DB_PROVIDER dataSourceProvider)throws UserDefinedException {
        if (dataSourceProvider.equals(Constants.DB_PROVIDER.POSTGRESQL)) {
            return new DictionaryDaoPostgresImpl(loggedUser);
        }

        throw new UnrecognizedOperationException();
    }



}
