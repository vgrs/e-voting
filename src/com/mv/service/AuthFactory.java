package com.mv.service;

import com.mv.dao.postgres.AuthServicePostgresImpl;
import com.mv.domain.exceptions.UnrecognizedOperationException;

public class AuthFactory {

    public static enum PROVIDER {
        USER_PASSWOR_POSTGRE,
        USER_PASSWOR_MYSQL,
    }

    public static AuthService getAuthProvider(PROVIDER authProvider) throws UnrecognizedOperationException{
        if (authProvider.equals(PROVIDER.USER_PASSWOR_POSTGRE)) {
            return new AuthServicePostgresImpl();
        }
        throw new UnrecognizedOperationException();
    }


}
