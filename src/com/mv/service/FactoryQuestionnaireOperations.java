package com.mv.service;

import com.mv.dao.QuestionnaireDao;
import com.mv.dao.postgres.QuestionnaireDaoPostgresImpl;
import com.mv.domain.User;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Constants;

/**
 * Created by a.maleyka on 15-Jun-17.
 */
public class FactoryQuestionnaireOperations {
    public static QuestionnaireDao getQuestionnaireOperationsProvider(User loggedUser, Constants.DB_PROVIDER dataSourceProvider) throws UserDefinedException{
        if(dataSourceProvider.equals(Constants.DB_PROVIDER.POSTGRESQL)){
            return new QuestionnaireDaoPostgresImpl(loggedUser);
        }
        throw new UnrecognizedOperationException();
    }
}
