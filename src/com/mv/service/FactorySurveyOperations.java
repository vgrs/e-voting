package com.mv.service;

import com.mv.dao.SurveyDao;
import com.mv.dao.postgres.SurveyDaoPostgresImpl;
import com.mv.domain.User;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Constants;

/**
 * Created by a.maleyka on 15-Jun-17.
 */
public class FactorySurveyOperations {
    public static SurveyDao getSurveyOperationsProvider(User loggedUser, Constants.DB_PROVIDER dataSourceProvider)throws UserDefinedException {
        if(dataSourceProvider.equals(Constants.DB_PROVIDER.POSTGRESQL)){
            return new SurveyDaoPostgresImpl(loggedUser);
        }
        throw new UnrecognizedOperationException();
    }
}
