package com.mv.controller.websocket;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv.dao.QuestionnaireDao;
import com.mv.domain.*;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.service.FactoryQuestionnaireOperations;
import com.mv.utils.Constants;
import com.mv.utils.JWT;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

@ServerEndpoint("/ws-end-point/{token}/{window}")
public class WebSocketEndPoint {

    private static final Map<Integer, Session> activeClients = Collections.synchronizedMap(new HashMap<Integer, Session>());
    private static final Map<Integer, Session> activeAdmins = Collections.synchronizedMap(new HashMap<Integer, Session>());
    private static final Map<Integer, Session> reportWindows = Collections.synchronizedMap(new HashMap<Integer, Session>());

    private static final Map<Integer, Integer> userConnectCounter = Collections.synchronizedMap(new HashMap<Integer, Integer>());


    private static final Map<Long,SurveyMemberQuestVariantAnswer> surveyMemberQuestVariantAnswers = Collections.synchronizedMap(new HashMap<Long, SurveyMemberQuestVariantAnswer>());

    private static Questionnaire questionnaire = null;

    private static  final char ACTION_DELIMITER = '.';
    private static boolean questionaireStarted = false;


    private static final Logger logger = FactoryLogger.getLogger();

    @OnOpen
    public void onOpen(Session session, @PathParam("token") String token, @PathParam("window") String window){
        logger.trace(session.getId() + " web socket session opened");
        try {
            session.getUserProperties().put(Constants.TOKEN, token);
            User user = JWT.checkToken4User(token);
            if (user == null){
                throw new UserDefinedException(0, "User is null");
            }

            if (user.getUserType() == User.UserTypes.Admin.value) {
                if (window.equals("admin")){
                    if (activeAdmins.get(user.getUserId()) != null){
                        throw new UserDefinedException(0, "admin User " + user.getUsername() + " is connected again and declined");
                    }

                    activeAdmins.put(user.getUserId(), session);
                    String serializedArray = Utils.serialize(new Integer[]{activeAdmins.keySet().size(), activeClients.keySet().size()});
                    for (Integer key : activeAdmins.keySet()){
                        Session ses = activeAdmins.get(key);
                        sendResponse2Session(ses, RequestHeader.ACTIONS.ADMIN_LOGIN, serializedArray);
                    }
                } else if(window.equals("report")){

                    if (reportWindows.get(user.getUserId()) != null ){
                        throw new UserDefinedException(0, "report window User " + user.getUsername() + " is connected again and declined");
                    }

                    reportWindows.put(user.getUserId(), session);

                    /*if (!questionaireStarted){
                        sendResponse2Session(session, RequestHeader.ACTIONS.SHOW_PRE_SCREEN);
                    } else {
                        sendResponse2Session(session, RequestHeader.ACTIONS.SHOW_MAIN_SCREEN);
                    }*/

                    if (questionnaire != null){
                        String serializeQuestionnaire = Utils.serialize(questionnaire);
                        sendResponse2Session(session, RequestHeader.ACTIONS.BROADCAST_QUESTION, serializeQuestionnaire);
                    }else {
                        sendResponse2Session(session, RequestHeader.ACTIONS.SHOW_PRE_SCREEN, user);
                    }

                    List<User> clients = new ArrayList<>(activeClients.keySet().size());
                    for (Integer key :  activeClients.keySet()){
                        Session s = activeClients.get(key);
                        String tt = (String) s.getUserProperties().get(Constants.TOKEN);
                        User client = JWT.checkToken4User(tt);
                        clients.add(client);
                    }

                    sendResponse2Session(session, RequestHeader.ACTIONS.BROADCAST_CONNECTED_CLIENTS, clients);

                    String serializedSurveyMemberQuestVariantAnswers = Utils.serialize(surveyMemberQuestVariantAnswers);
                    sendResponse2Session(session, RequestHeader.ACTIONS.ANSWER_STATISTICS, serializedSurveyMemberQuestVariantAnswers);

                }
            } else {
                if (activeClients.get(user.getUserId()) != null ){
                    throw new UserDefinedException(0, "client User " + user.getUsername() + " is connected again and declined");
                }

                activeClients.put(user.getUserId(), session);

                /*if (!questionaireStarted){

                } else {
                    sendResponse2Session(session, RequestHeader.ACTIONS.SHOW_MAIN_SCREEN);
                }*/

                if (questionnaire != null){
                    String serializeQuestionnaire = Utils.serialize(questionnaire);
                    sendResponse2Session(session, RequestHeader.ACTIONS.BROADCAST_QUESTION, serializeQuestionnaire);
                }else {
                    sendResponse2Session(session, RequestHeader.ACTIONS.SHOW_PRE_SCREEN, user);
                }

                for (Integer key : activeAdmins.keySet()){
                    Session ses = activeAdmins.get(key);
                    sendResponse2Session(ses, RequestHeader.ACTIONS.CLIENT_LOGIN, activeClients.keySet().size());
                }
            }
        } catch (AuthenticationException e) {
            logger.error(e);
        } catch (UserDefinedException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        } catch (Exception e){
            logger.error(e);
        }
    }


    @OnMessage
    public void onMessage(String message, Session session, @PathParam("token") String token) throws IOException {
        try {
            User user = JWT.checkToken4User(token);
            ObjectMapper mapper = new ObjectMapper();
            int ind = message.indexOf(ACTION_DELIMITER);
            String action = message.substring(0, ind);
            String msgBody = message.substring(ind + 1);
            logger.trace("message = " + message);
            if (action.equals(RequestHeader.ACTIONS.BROADCAST_QUESTION.name())){
                questionnaire = mapper.readValue(msgBody, Questionnaire.class);
                questionnaire.setBroadCastTime(new Date());
                String serializeQuestionnaire = Utils.serialize(questionnaire);

                broadCastQuestion(serializeQuestionnaire, activeClients);
                /*for(Integer key : activeClients.keySet()){
                    sendResponse2Session(activeClients.get(key), RequestHeader.ACTIONS.BROADCAST_QUESTION_START);
                    sendResponse2Session(activeClients.get(key), RequestHeader.ACTIONS.BROADCAST_QUESTION, serializeQuestionnaire);
                }*/

                broadCastQuestion(serializeQuestionnaire, reportWindows);
                /*for(Integer key : reportWindows.keySet()){
                    sendResponse2Session(reportWindows.get(key), RequestHeader.ACTIONS.BROADCAST_QUESTION_START);
                    sendResponse2Session(reportWindows.get(key), RequestHeader.ACTIONS.BROADCAST_QUESTION, serializeQuestionnaire);
                }*/

                broadCastQuestion(serializeQuestionnaire, activeAdmins);
                /*for(Integer key : activeAdmins.keySet()){
                    sendResponse2Session(activeAdmins.get(key), RequestHeader.ACTIONS.BROADCAST_QUESTION, serializeQuestionnaire);
                }*/

                TimerControl timerControl = new TimerControl();
                timerControl.timer(new TimedQuestionnaire(), questionnaire.getTimeInterval());

                questionaireStarted = true;
            } else if ( action.equals(RequestHeader.ACTIONS.ANSWER.name()) ){
                logger.trace("inside action.equals(RequestHeader.ACTIONS.ANSWER.name()");
                JsonNode node = mapper.readTree(msgBody);
                JsonNode node2 = node.get("questionnaire_movement_id");
                long questionnaire_movement_id = node2.asLong();
                JsonNode node3 = node.get("questionnaire_answers");
                String s = mapper.writeValueAsString(node3);
                logger.trace(s);

                QuestionnaireAnswerWrapper answerWrapper = mapper.readValue(s, QuestionnaireAnswerWrapper.class);

                SurveyMemberQuestVariantAnswer ee = new SurveyMemberQuestVariantAnswer();
                ee.setQuestionnaireAnswerList(answerWrapper.getQuestionnaireAnswerList());
                ee.setSurveyMember(user.getSurveyMember());
                ee.setQuestionnaireMovementId(questionnaire_movement_id);
                surveyMemberQuestVariantAnswers.remove(user.getSurveyMember().getId());
                surveyMemberQuestVariantAnswers.put(user.getSurveyMember().getId(), ee);

                Response<Boolean> response = answerQuestionnaire(user, node2.asLong(), answerWrapper.getQuestionnaireAnswerList());

                String serialize = Utils.serialize(response);
                sendResponse2Session(session, RequestHeader.ACTIONS.ANSWER, response);
                logger.trace("response : " + serialize);
                if (response.getResultMessage().getResultStatus().equals(Constants.ResultStatus.SUCCESS)){
                    String serializedEE =  Utils.serialize(ee);
                    for (Integer key :  reportWindows.keySet()){
                        sendResponse2Session(reportWindows.get(key), RequestHeader.ACTIONS.ANSWER, serializedEE);
                    }
                }

            } else if (action.equals(RequestHeader.ACTIONS.GET_ADMINS.name())){
                List<User> users = new ArrayList<>();
                for (Integer key : activeAdmins.keySet() ){
                    Session ses = activeAdmins.get(key);
                    String t =  (String) ses.getUserProperties().get(Constants.TOKEN);
                    User u = JWT.checkToken4User(t);
                    users.add(u);
                }
                sendResponse2Session(session, RequestHeader.ACTIONS.GET_ADMINS, users);
            } else if (action.equals(RequestHeader.ACTIONS.GET_CLIENTS.name())){
                List<User> users = new ArrayList<>();
                for ( Integer key : activeClients.keySet() ){
                    Session ses = activeClients.get(key);
                    String t =  (String) ses.getUserProperties().get(Constants.TOKEN);
                    User u = JWT.checkToken4User(t);
                    users.add(u);
                }

                sendResponse2Session(session, RequestHeader.ACTIONS.GET_CLIENTS, users);
            } else if(action.equals(RequestHeader.ACTIONS.CLOSE_SURVEY.name())){
                for ( Integer key : activeClients.keySet() ){
                    Session ses = activeClients.get(key);
                    sendResponse2Session(ses, RequestHeader.ACTIONS.CLOSE_SURVEY);
                }

                for ( Integer key : reportWindows.keySet() ){
                    Session ses = reportWindows.get(key);
                    sendResponse2Session(ses, RequestHeader.ACTIONS.SHOW_PRE_SCREEN);
                }
            }
        } catch (IOException ex) {
            logger.error(ex);
            Response response = new Response();
            response.initFrom(new UserDefinedException(0, ex.getMessage()));
            String serialize = Utils.serialize(response);
            session.getBasicRemote().sendText(serialize);
        } catch (AuthenticationException e) {
            logger.error(e);
            Response response = new Response();
            response.initFrom(e);
            String serialize = Utils.serialize(response);
            session.getBasicRemote().sendText(serialize);
        } catch (Exception e){
            logger.error(e);
            Response response = new Response();
            response.setResultMessage(new ResultMessage(Constants.ResultStatus.ERROR, new Message(0, e.getMessage())));
            String serialize = Utils.serialize(response);
            session.getBasicRemote().sendText(serialize);
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("token") String t, @PathParam("window") String window){
        String token = (String)session.getUserProperties().get("token");
        logger.trace();
        try {
            User user = JWT.checkToken4User(token);
            if (user == null ){
                throw new UserDefinedException(0, "User is null");
            }
            session.getUserProperties().put("active", 0);

            if ( user.getUserType() == User.UserTypes.Admin.value){
                if (window.equals("admin")){
                    activeAdmins.remove(user.getUserId());
                    reportWindows.remove(user.getUserId());
                    logger.trace("Admin " + user.getUsername() + " is removed from active admins");
                    Integer[] integers = {activeAdmins.keySet().size(), activeClients.keySet().size()};
                    String serializedArray = Utils.serialize(integers);
                    for (Integer key : activeAdmins.keySet()){
                        try {
                            Session ses = activeAdmins.get(key);
                            sendResponse2Session(ses, RequestHeader.ACTIONS.ADMIN_CLOSE, serializedArray);
                        } catch (IOException e){
                            logger.error(e);
                        }
                    }
                } else if (window.equals("report")){
                    reportWindows.remove(user.getUserId());
                }
            } else {
                activeClients.remove(user.getUserId());

                logger.trace("Client " + user.getUsername() + " is removed from active clients");

                Integer[] integers = {activeAdmins.keySet().size(), activeClients.keySet().size()};
                String serializedArray = Utils.serialize(integers);
                for (Integer key : activeAdmins.keySet()){
                    try{
                        Session ses = activeAdmins.get(key);
                        sendResponse2Session(ses, RequestHeader.ACTIONS.CLIENT_CLOSE, serializedArray);
                    } catch (IOException e){
                        logger.error(e);
                    }
                }
            }

        } catch (UserDefinedException e) {
            logger.error(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class TimedQuestionnaire implements RunnableWithEnd{

        @Override
        public void run() {
            long seconds = questionnaire.getTimeInterval() - ((new Date().getTime() - questionnaire.getBroadCastTime().getTime())/1000);
            String secondsStr = String.valueOf(seconds);

            logger.trace("RequestHeader.ACTIONS.BROADCAST_TIME(seconds = "+ secondsStr +") started loop activeClients. questionnaireMovementId = " + questionnaire.getQuestionnaireMovementId());
            broadCastTime(secondsStr, activeClients);

            logger.trace("RequestHeader.ACTIONS.BROADCAST_TIME(seconds = "+ secondsStr +") started loop reportWindows. questionnaireMovementId = " + questionnaire.getQuestionnaireMovementId());
            broadCastTime(secondsStr, reportWindows);

            logger.trace("RequestHeader.ACTIONS.BROADCAST_TIME(seconds = "+ secondsStr +") started loop activeAdmins. questionnaireMovementId = " + questionnaire.getQuestionnaireMovementId());
            broadCastTime(secondsStr, activeAdmins);
        }


        @Override
        public void end() {
            questionnaire = null;
            surveyMemberQuestVariantAnswers.clear();
            broadCastPreScreen(activeClients);
            broadCastPreScreen(reportWindows);
        }
    }

    private void broadCastPreScreen(Map<Integer, Session> map){
        for(Integer key : map.keySet()){
            try {
                Session session = map.get(key);
                String token = (String)session.getUserProperties().get(Constants.TOKEN);
                User user = JWT.checkToken4User(token);
                sendResponse2Session(map.get(key), RequestHeader.ACTIONS.SHOW_PRE_SCREEN, user);
            } catch (IOException e) {
                logger.error(e);
            } catch (AuthenticationException e) {
                e.printStackTrace();
            }
        }
    }

    private void broadCastQuestion(String serializeQuestionnaire, Map<Integer, Session> map) throws IOException {
        for(Integer key : map.keySet()){
            sendResponse2Session(map.get(key), RequestHeader.ACTIONS.BROADCAST_QUESTION_START);
            sendResponse2Session(map.get(key), RequestHeader.ACTIONS.BROADCAST_QUESTION, serializeQuestionnaire);
        }
    }

    private void broadCastTime(String secondsStr , Map<Integer, Session> map){
        for ( Integer key : map.keySet() ){
            try {
                Session ses = map.get(key);
                logger.trace("RequestHeader.ACTIONS.BROADCAST_TIME sending questionnaireMovementId " + questionnaire.getQuestionnaireMovementId() + " to client " + key);
                sendResponse2Session(ses, RequestHeader.ACTIONS.BROADCAST_TIME, secondsStr);
            } catch (IOException io){
                logger.error(io);
            }
        }
    }

    private Response<Boolean> answerQuestionnaire( User user, long questionnaire_movement_id, QuestionnaireAnswer [] questionnaireAnswerList) throws UserDefinedException, IOException {
        QuestionnaireDao questionnaireDao = FactoryQuestionnaireOperations.getQuestionnaireOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
        Boolean bool = questionnaireDao.answerQuestionnaire(questionnaire_movement_id, questionnaireAnswerList);
        Response<Boolean> booleanResponse= new Response<>();
        booleanResponse.setData(bool);
        booleanResponse.setResultMessage(new ResultMessage (Constants.ResultStatus.SUCCESS, new Message(0, "Questionnaire answered")));
        return booleanResponse;
    }

    private void sendResponse2Session(Session session, RequestHeader.ACTIONS action, Object message) throws IOException {
        session.getBasicRemote().sendText(action.name() + ACTION_DELIMITER + Utils.serialize(message));
    }
    private void sendResponse2Session(Session session, RequestHeader.ACTIONS action, String message) throws IOException {
        session.getBasicRemote().sendText(action.name() + ACTION_DELIMITER + message);
    }
    private void sendResponse2Session(Session session, RequestHeader.ACTIONS action) throws IOException {
        session.getBasicRemote().sendText(action.name() + ACTION_DELIMITER );
    }
}

interface RunnableWithEnd extends Runnable{

    public void end();
}

class TimerControl {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public void timer(final RunnableWithEnd timer , long x ) {

        final ScheduledFuture<?> handle = scheduler.scheduleAtFixedRate(timer, 1, 1, SECONDS);

        scheduler.schedule(new Runnable() {
            public void run() {
                timer.end();
                handle.cancel(true);
                scheduler.shutdownNow();
            }
        }, x, SECONDS);
    }

}

class RequestHeader implements Serializable{

    public static enum ACTIONS{
        ANSWER("answer"),
        BROADCAST_QUESTION("broadcast-question"),
        BROADCAST_QUESTION_START("broadcast-question-start"),
        BROADCAST_TIME("broadcast-time"),
        ADMIN_LOGIN("admin-login"),
        CLIENT_LOGIN("client-login"),
        SHOW_PRE_SCREEN("show-pre-screen"),
        SHOW_MAIN_SCREEN("show-waiting-screen"),
        ANSWER_STATISTICS("answer-statistics"),
        BROADCAST_CONNECTED_CLIENTS("broadcast-connected-clients"),

        ADMIN_CLOSE("admin-login"),
        CLIENT_CLOSE("client-login"),

        GET_ADMINS("get-admins"),
        GET_CLIENTS("get-clients"),

        CLOSE_SURVEY("CLOSE-SURVEY");

        private String value;

        ACTIONS(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    private String token;
    private ACTIONS action;

    public RequestHeader() {

    }

    public RequestHeader(String token, ACTIONS action) {
        this.token = token;
        this.action = action;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public ACTIONS getAction() {
        return action;
    }

    public void setAction(ACTIONS action) {
        this.action = action;
    }

}


class SurveyMemberQuestVariantAnswer implements Serializable{
    private long questionnaireMovementId;
    private SurveyMember surveyMember;

    private QuestionnaireAnswer [] questionnaireAnswerList;

    public long getQuestionnaireMovementId() {
        return questionnaireMovementId;
    }

    public void setQuestionnaireId(long questionnaireMovementId) {
        this.questionnaireMovementId = questionnaireMovementId;
    }

    public SurveyMember getSurveyMember() {
        return surveyMember;
    }

    public void setSurveyMember(SurveyMember surveyMember) {
        this.surveyMember = surveyMember;
    }

    public void setQuestionnaireMovementId(long questionnaireMovementId) {
        this.questionnaireMovementId = questionnaireMovementId;
    }

    public QuestionnaireAnswer[] getQuestionnaireAnswerList() {
        return questionnaireAnswerList;
    }

    public void setQuestionnaireAnswerList(QuestionnaireAnswer[] questionnaireAnswerList) {
        this.questionnaireAnswerList = questionnaireAnswerList;
    }
}
