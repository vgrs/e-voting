package com.mv.controller.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv.dao.SurveyDao;
import com.mv.dao.UserDao;
import com.mv.dao.postgres.SurveyDaoPostgresImpl;
import com.mv.domain.*;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.service.FactorySurveyOperations;
import com.mv.service.FactoryUsersOperations;
import com.mv.utils.Constants;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;
import com.mv.utils.serializers.DataContentSerializer;
import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;


@WebServlet(name = "UsersServlet")
public class SurveyServlet extends HttpServlet {
    Logger logger = FactoryLogger.getLogger();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public void processRequest(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        response.setContentType("application/json");
        OutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            HttpSession session = request.getSession();
            User user = (User)session.getAttribute(Constants.LOGGED_USER);
            if (user == null) throw new AuthenticationException();

            String subAction = request.getParameter(Constants.SUB_ACTION);
            if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.addSurvey.name())){
                logger.trace();
                SurveyDao surveyDao = FactorySurveyOperations.getSurveyOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                String json = request.getParameter("survey");
                ObjectMapper objectMapper = new ObjectMapper();
                Survey survey = objectMapper.readValue(json, Survey.class);
                Long surveyId = surveyDao.addSurvey(survey);
                Response<Long> integerResponse = new Response<>();
                integerResponse.setData(surveyId);
                integerResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "Survey added")));
                logger.trace();
                Utils.serializeAndWrite2(integerResponse, outputStream);
                logger.trace();
            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.editSurvey.name())){
                logger.trace();
                SurveyDao surveyDao = FactorySurveyOperations.getSurveyOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                String json = request.getParameter("survey");
                ObjectMapper objectMapper = new ObjectMapper();
                Survey survey = objectMapper.readValue(json, Survey.class);

                boolean is = surveyDao.editSurvey(survey);
                Response<Boolean> integerResponse = new Response<>();
                integerResponse.setData(is);
                integerResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "Survey edited")));
                logger.trace();
                Utils.serializeAndWrite2(integerResponse, outputStream);
                logger.trace();
            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.deleteSurvey.name())){
                logger.trace();
                SurveyDao surveyDao = FactorySurveyOperations.getSurveyOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                String survey_id = request.getParameter("survey_id");
                boolean is = surveyDao.deleteSurvey(Long.parseLong(survey_id));
                Response<Boolean> integerResponse = new Response<>();
                integerResponse.setData(is);
                integerResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "Survey deleted")));
                logger.trace();
                Utils.serializeAndWrite2(integerResponse, outputStream);
                logger.trace();
            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.stopSurvey.name())){
                logger.trace();
                SurveyDao surveyDao = FactorySurveyOperations.getSurveyOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                long id = Long.parseLong(request.getParameter("survey_id"));
                Boolean bool = surveyDao.stopSurvey(id);
                Response<Boolean> longResponse = new Response<>();
                longResponse.setData(bool);
                longResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "Survey stopped")));
                Utils.serializeAndWrite2(longResponse, outputStream);
                logger.trace();
            } else if (subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.getSurveyDetails.name())){
                SurveyDao surveyDao = FactorySurveyOperations.getSurveyOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                long surveyId = Long.parseLong(request.getParameter("survey_id"));

                DataContent[] surveyInfo = surveyDao.getSurveyInfo(surveyId);
                Utils.serializeAndWrite2(surveyInfo, outputStream);

            } else if (subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.getCurrentSurveyDetails.name())){
                SurveyDao surveyDao = FactorySurveyOperations.getSurveyOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                DataContent[] surveyInfo = surveyDao.getCurrentSurveyInfo();
                Utils.serializeAndWrite2(surveyInfo, outputStream);

            }
            else {
                throw new UnrecognizedOperationException();
            }
            logger.trace();
        } catch (UserDefinedException e){
            logger.error(e);
            Response resp = new Response();
            resp.initFrom(e);
            Utils.serializeAndWrite2(resp, outputStream);
        } catch (Exception e){
            logger.error(e);
            Response resp = new Response();
            resp.initFrom(new UserDefinedException(0,e.getMessage()));
            Utils.serializeAndWrite2(resp, response.getOutputStream());
        } finally {
            if (outputStream != null){
                outputStream.close();
            }
        }

    }

}
