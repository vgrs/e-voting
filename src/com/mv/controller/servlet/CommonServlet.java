package com.mv.controller.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mv.dao.DictionaryDao;
import com.mv.dao.QuestionnaireDao;
import com.mv.dao.ReportDao;
import com.mv.dao.SurveyDao;
import com.mv.dao.postgres.ReportDaoPostgreImpl;
import com.mv.domain.*;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.service.FactoryDictionaryOperations;
import com.mv.service.FactoryQuestionnaireOperations;
import com.mv.service.FactorySurveyOperations;
import com.mv.utils.Constants;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;
import com.mv.utils.serializers.DataContentSerializer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;


public class CommonServlet extends HttpServlet {

    Logger logger = FactoryLogger.getLogger();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public void processRequest(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        OutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();

            HttpSession session = request.getSession();
            User user = (User)session.getAttribute(Constants.LOGGED_USER);
            if (user == null) throw new AuthenticationException();

            String subAction = request.getParameter(Constants.SUB_ACTION);
            if (subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.getDataContent.name())){
                String type = request.getParameter("type");
                logger.trace("getDataContent : type = " + type);
                if (type.equalsIgnoreCase("getDictionaries")){
                    DictionaryDao dictionaryDao = FactoryDictionaryOperations.getDictionaryOperationProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                    int typeId = Integer.parseInt(request.getParameter("typeId"));
                    DataContent dictionaries = dictionaryDao.getDictionaries(typeId);
                    Utils.serializeAndWrite2(DataContent.class, new DataContentSerializer(), dictionaries, outputStream);
                } else if (type.equalsIgnoreCase("getCountries")){
                    DictionaryDao dictionaryDao = FactoryDictionaryOperations.getDictionaryOperationProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                    DataContent dictionaries = dictionaryDao.getCountries();
                    Utils.serializeAndWrite2(DataContent.class, new DataContentSerializer(), dictionaries, outputStream);
                } else if (type.equalsIgnoreCase("getSurveys")){
                    SurveyDao surveyDao = FactorySurveyOperations.getSurveyOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                    DataContent surveys = surveyDao.getSurvey();
                    Utils.serializeAndWrite2(DataContent.class, new DataContentSerializer(), surveys, outputStream);
                }else if(type.equalsIgnoreCase("getQuestionnaires")){
                    QuestionnaireDao questionnaireDao = FactoryQuestionnaireOperations.getQuestionnaireOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                    int typeId = Integer.parseInt(request.getParameter("typeId"));
                    int privacyType = Integer.parseInt(request.getParameter("privacyType"));
                    String startDate = request.getParameter("startDate");
                    int surveyId = Integer.parseInt(request.getParameter("survey_id"));
                    DataContent questionnaries = questionnaireDao.getQuestionnaire(typeId, privacyType, startDate, surveyId);
                    Utils.serializeAndWrite2(DataContent.class, new DataContentSerializer(), questionnaries, outputStream);
                } else if(type.equalsIgnoreCase("getQuestionnaireDetail")){
                    QuestionnaireDao questionnaireDao = FactoryQuestionnaireOperations.getQuestionnaireOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                    int id = Integer.parseInt(request.getParameter("id"));
                    DataContentArray questionnaireDetail = questionnaireDao.getQuestionnaireDetail(id);
                    Utils.serializeAndWrite2(DataContent.class, new DataContentSerializer(), questionnaireDetail, outputStream);
                } else if(type.equalsIgnoreCase("getQuestionnaireDetail2")){
                    QuestionnaireDao questionnaireDao = FactoryQuestionnaireOperations.getQuestionnaireOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                    long id = Long.parseLong(request.getParameter("id"));
                    DataContent[] questionnaireDetail2 = questionnaireDao.getQuestionnaireDetail2(id);
                    Utils.serializeAndWrite2(questionnaireDetail2, outputStream);
                } else if(type.equalsIgnoreCase("getReportBySurvey")){
                    ReportDao reportDao = new ReportDaoPostgreImpl();
                    int surveyId = Integer.parseInt(request.getParameter("survey_id")== null ? "0" : request.getParameter("survey_id"))  ;
                    DataContent reports = reportDao.getQuestResult(surveyId);
                    Utils.serializeAndWrite2(DataContent.class, new DataContentSerializer(), reports, outputStream);
                }else if(type.equalsIgnoreCase("getReportByQuest")){
                    ReportDao reportDao = new ReportDaoPostgreImpl();
                    String s = request.getParameter("quest_id");
                    System.out.println(s);
                    int questId = Integer.parseInt(s);
                    DataContent[] reports = reportDao.getResultByQuest(questId);
                    Utils.serializeAndWrite2(reports, outputStream);
                }

            } else {
                throw new UnrecognizedOperationException();
            }

        }catch (UserDefinedException e){
            logger.error(e);
            Response resp = new Response();
            resp.initFrom(e);
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), resp);
        }catch (Exception e){
            logger.error(e);
            Response resp = new Response();
            resp.setResultMessage(new ResultMessage(Constants.ResultStatus.ERROR, new Message(0, e.getMessage())));
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), resp);
        }

    }


}
