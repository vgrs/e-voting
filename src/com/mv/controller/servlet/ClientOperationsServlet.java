package com.mv.controller.servlet;

import com.mv.domain.Response;
import com.mv.domain.User;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Constants;
import com.mv.utils.JWT;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;


@WebServlet(name = "UsersServlet")
public class ClientOperationsServlet extends HttpServlet {

    Logger logger = FactoryLogger.getLogger();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public void processRequest(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{

        response.setContentType("application/json");
        try (OutputStream outputStream = response.getOutputStream()){
            HttpSession session = request.getSession();
            User user = (User)session.getAttribute(Constants.LOGGED_USER);
            if (user == null) throw new AuthenticationException();

            String subAction = request.getParameter(Constants.SUB_ACTION);
            if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.getWebSocketUrl.name())){
                request.getContextPath();
                String url = "ws://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()
                        + "/ws-end-point/" + JWT.buildToken(user, false);

                Response<String> res = new Response<>();
                res.setData(url);
                Utils.serializeAndWrite2(res, outputStream);
            } else {
                throw new UnrecognizedOperationException();
            }

        }catch (UserDefinedException e){
            logger.error(e);
            Response resp = new Response();
            resp.initFrom(e);
            Utils.serializeAndWrite2(resp, response.getOutputStream());
        }catch (Exception e){
            logger.error(e);
        }

    }

}
