package com.mv.controller.servlet;

import com.mv.domain.*;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.service.AuthFactory;
import com.mv.service.AuthService;
import com.mv.utils.Constants;
import com.mv.utils.JWT;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(name = "AuthenticationServlet")
public class AuthenticationServlet extends HttpServlet {

    Logger logger = FactoryLogger.getLogger();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }


    public void processRequest(HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException {
        HttpSession httpSession = request.getSession();
        try {

            String action = request.getServletPath();
            if (action.equalsIgnoreCase("/log-in")){
                String username = request.getParameter("username");
                String pwd = request.getParameter("password");
                UserPasswordCredentials credentials = new UserPasswordCredentials(username, pwd);
                AuthService authService = AuthFactory.getAuthProvider(AuthFactory.PROVIDER.USER_PASSWOR_POSTGRE);
                User user = authService.authenticate(credentials);

                httpSession.setAttribute(Constants.LOGGED_USER, user);
                if (user.getUserType() == User.UserTypes.Admin.value){
                    response.sendRedirect(Constants.PAGE_ADMIN);
                }else {
                    response.sendRedirect(Constants.PAGE_CLIENT);
                }
            } else if (action.equalsIgnoreCase("/log-out")){
                httpSession.invalidate();
                response.sendRedirect(Constants.PAGE_LOGIN );
            } else if (action.equalsIgnoreCase("/get-token")){
                User loggerUser = (User) httpSession.getAttribute(Constants.LOGGED_USER);
                if (loggerUser == null){
                    throw new AuthenticationException();
                }
                String token = JWT.buildToken(loggerUser);
                Response<String> res = new Response<>();
                res.setData(token);
                Utils.serializeAndWrite2(res, response.getOutputStream());
            } else {
                throw new UnrecognizedOperationException();
            }
        } catch (AuthenticationException au){
            logger.error(au);
            httpSession.setAttribute(Constants.ERROR_MESSAGE, "User name or password is incorrect");
            response.sendRedirect(Constants.PAGE_LOGIN);
        } catch (Exception e){
            logger.error(e);
            httpSession.setAttribute(Constants.ERROR_MESSAGE, "Error during login : " + e.getMessage());
            response.sendRedirect(Constants.PAGE_LOGIN);
        }

    }

}
