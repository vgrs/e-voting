package com.mv.controller.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "UrlServlet")
public class UrlServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String servletPath = request.getServletPath();
        String initParameter = getInitParameter(servletPath);
        if (initParameter == null)  response.sendError(HttpServletResponse.SC_NOT_FOUND);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(initParameter);
        requestDispatcher.forward(request, response);
    }

}
