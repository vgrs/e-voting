package com.mv.controller.filter;

import com.mv.domain.User;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.utils.Constants;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebFilter(filterName = "AuthorizationFilter")
public class AuthorizationFilter implements Filter {

    Logger logger = FactoryLogger.getLogger();

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse)resp;
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Constants.LOGGED_USER);
        try {
            if (user == null){
                throw new AuthenticationException();
            }
            if (!user.getPathAccess(request.getServletPath())){
                throw new AuthenticationException();
            }

            if ( request.getServletPath().equals(Constants.SERVLET_PATHS.LOGIN.value) ){
                if (user.getUserType() == User.UserTypes.Admin.value){
                    response.sendRedirect(Constants.PAGE_ADMIN);
                } else {
                    response.sendRedirect(Constants.PAGE_CLIENT);
                }
            }else {
                chain.doFilter(req, resp);
            }
        } catch (AuthenticationException a){
            logger.error(a);
            request.getRequestDispatcher(Constants.PAGE_LOGIN).forward(request, response);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
